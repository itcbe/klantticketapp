﻿var registraties = [];
var Drempelwaarde = 0;
var regisIndex = 0;
var globaldatumvanaf = null;
var globaltechnieker = null;
var globalticketnummer = null;
var globalStatus = null;
var globalAflevernota = null;
var globalLocaties = [];
var ticketOmschrijvingen = [];

function Registratie() {
    this.ID;
    this.ticketID;
    this.klantID;
    this.ticketOmschrijving;
    this.Locatie;
    this.startuur;
    this.einduur;
    this.duur;
    this.facturatieTekst;
    this.uitvoeder;
    this.onsite;
    this.uren;
    this.permanentie;
    this.typeWerk;
    this.status;
    this.eenheidsprijs;
    this.briljant;
    this.opmFactuur;
}

function GetRegistratieOpTicket(id) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View><Query>" 
                + "<Where>"
                + "<And>"
                + "<Eq><FieldRef Name='Title' /><Value Type='Text'>" + id + "</Value></Eq>"
                + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                + "</And>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='ID' /></OrderBy>" 
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Datum' />"
                + "<FieldRef Name='Omschrijving' />"
                + "<FieldRef Name='Onsite' />"
                + "<FieldRef Name='Permanentie' />"
                + "<FieldRef Name='Korte_x0020_omschrijving' />"
                + "<FieldRef Name='Author' />"
                + "</ViewFields>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);


        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
            function () {
                var listEnumerator = _pendingItems.getEnumerator();
                var  registraties = [];       
                while (listEnumerator.moveNext()) {
                    var item = listEnumerator.get_current();
                    var datum = ToNormalDate(new Date(item.get_item('Datum')));

                    registraties.push({
                        ID: item.get_item('ID'),
                        Datum: datum,
                        Omschrijving: item.get_item('Korte_x0020_omschrijving'),
                        Onsite: item.get_item('Onsite') == true ? "Ja" : "Nee",
                        Permanentie: item.get_item('Permanentie') == true ? "Ja" : "Nee",
                        Door: item.get_item('Author').get_lookupValue()
                    });
                }
                BindRegistratiesToPopup(registraties, id);
                dfd.resolve();
            },
            function (sender, args) {
                alert('Kan de tijdsregistraties niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
                dfd.reject();
            });

    });
    return dfd.promise();
}

function BindRegistratiesToPopup(regis, id) {
    $('#tijdsregistratieTBody').empty();
    if (regis.length > 0) {
        for (var i = 0; i < regis.length; i++) {
            var row = "<tr class='tijdsregistratiesRij'><td>" + regis[i].Datum + "</td>";
            row += "<td>" + regis[i].Omschrijving + "</td>";
            row += "<td>" + regis[i].Onsite + "</td>";
            row += "<td>" + regis[i].Permanentie + "</td>";
            row += "<td>" + regis[i].Door + "</td></tr>";
            $('#tijdsregistratieTBody').append(row);
        }
        // show popup
        var titel = "Uitgevoerde taken op ticket " + id + ".";
        $('#TijdsregistratiePopup').dialog({
            width: 1300,
            modal: true,
            open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
            title: titel,
            hide: {
                effect: "fade",
                duration: 500
            },
            show: {
                effect: "fade",
                duration: 500
            },
            maxHeight: 700,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    else {
        alert("Er zijn nog geen taken uitgevoerd op dit ticket.");
    }
}

function GetLedenVanGroep(klantid) {
    globalLocaties = [];
    var nummers = [];
    $('#groepTbody tr').each(function () {
        $tr = $(this);
        var id = $tr.find('td:nth-child(1)').text();
        var Locatie = {
            ID: id,
            Loc: $tr.find('td:nth-child(4)').text()
        };
        globalLocaties.push(Locatie);
        nummers.push(id);
    });
    if (nummers.length < 1)
        nummers.push(klantid);
    return nummers;
}

function GetAllRegistraties() {
    klantid = $("#klantenSelect").val();
    var klantnummers = GetLedenVanGroep(klantid);
    registraties = [];
    Drempelwaarde = 0;
    regisIndex = 0;
    globaldatumvanaf = null;
    globaltechnieker = null;
    globalticketnummer = null;
    globalStatus = null;
    globalAflevernota = null;

    klantid = $("#klantenSelect").val();
    if ($("#vanafDatePicker").val() !== "")
        globaldatumvanaf = ToSharePointDate($("#vanafDatePicker").val());
    else
        globaldatumvanaf = ToSharePointDate("01/01/2010");
    if ($("#ticketnummerTextBox").val() !== "")
        globalticketnummer = $("#ticketnummerTextBox").val();
    if ($("#uitgevoerdDoorSelect option:selected").text() !== "")
        globaltechnieker = $("#uitgevoerdDoorSelect option:selected").text();
    if ($("#statusSelect option:selected").text() !== "")
        globalStatus = $("#statusSelect option:selected").text();
    if ($("#aflevernotaSelect option:selected").text() !== "") {
        var temp = $("#aflevernotaSelect option:selected").text();
        if (temp == "Ja")
            globalAflevernota = 1;
        else
            globalAflevernota = 0;

    }

    GetTijdsregistraties(globaldatumvanaf, Drempelwaarde, globaltechnieker, globalticketnummer, klantnummers, globalStatus, globalAflevernota, 0);
}

function GetTijdsregistraties(datum, drempelwaarde, technieker, ticket, klantnummers, status, aflevernota, index) {
    var self = this;
    
    self.loadList = function () {
        var minValue = drempelwaarde;
        var maxValue = minValue + 4999;
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View><Query>";
        caml += "<Where>";
        if (ticket != null)
            caml += "<And>";

        if (technieker != null)
            caml += "<And>";

        if (status != null)
            caml += "<And>";

        if (aflevernota != null)
            caml += "<And>";

        //+  "<And>");
        caml += "<And>";
        caml += "<And>";
        caml += "<And>";
        caml += "<Geq><FieldRef Name='ID'/><Value Type='Counter'>" + minValue + "</Value></Geq>";
        caml += "<Lt><FieldRef Name='ID'/><Value Type='Counter'>" + maxValue + "</Value></Lt>";
        caml += "</And>";
        caml += "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantnummers[index] + "</Value></Eq>";
        caml += "</And>";
        caml += "<Gt><FieldRef Name='Datum' /><Value Type='Calculated'>" + datum + "</Value></Gt>";
        caml += "</And>";
        //+  "<Eq><FieldRef Name='Aflevernote' /><Value Type='Boolean'>0</Value></Eq>");
        //+  "</And>");

        if (ticket != null)
        {
            caml += "<Eq><FieldRef Name='Title' /><Value Type='Text'>" + ticket + "</Value></Eq>";
            caml += "</And>";
        }

        if (technieker != null)
        {
            caml += "<Eq><FieldRef Name='Author' /><Value Type='Text'>" + technieker + "</Value></Eq>";
            caml += "</And>";
        }

        if (status != null) {
            caml += "<Eq><FieldRef Name='Status' /><Value Type='Choise'>" + status + "</Value></Eq>";
            caml += "</And>";
        }

        if (aflevernota != null) {
            caml += "<Eq><FieldRef Name='Aflevernote' /><Value Type='Boolean'>" + aflevernota + "</Value></Eq>";
            caml += "</And>";
        }

        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Title' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Startuur' />";
        caml += "<FieldRef Name='Einduur' />";
        caml += "<FieldRef Name='Duur' />";
        caml += "<FieldRef Name='Korte_x0020_omschrijving' />";
        caml += "<FieldRef Name='Onsite' />";
        caml += "<FieldRef Name='Permanentie' />";
        caml += "<FieldRef Name='Datum' />";
        caml += "<FieldRef Name='Type_x0020_werk' />";
        caml += "<FieldRef Name='Status' />";
        caml += "<FieldRef Name='Te_x0020_factureren_x0020_uren' />";
        caml += "<FieldRef Name='Eenheidsprijs' />";
        caml += "<FieldRef Name='Aflevernote' />";
        caml += "<FieldRef Name='Uren' />";
        caml += "<FieldRef Name='Author' />";
        caml += "<FieldRef Name='Opm_x0020_facturatie' /></ViewFields> ";
        caml += "<RowLimit>4999</RowLimit>";
        caml += " </View>";



        var query = new SP.CamlQuery();
        query.set_viewXml(caml);


        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaleDuur = 0;       
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = ToDateString(item.get_item('Startuur').toString(), false);
            var einduur = ToDateString(item.get_item('Einduur').toString(), false);
            var auteur = item.get_item('Author').get_lookupValue();
            var regis = new Registratie();
            //var klant = item.get_item('Klant');

            regis.ID = item.get_item('ID');
            regis.ticketID = item.get_item('Title');
            ///if (klant)
            //regis.Klant = klant.get_lookupValue();
            regis.klantID = klantnummers[index];
            for (var i = 0; i < globalLocaties.length; i++) {
                if (globalLocaties[i].ID === klantnummers[index]) {
                    regis.Locatie = globalLocaties[i].Loc;
                    break;
                }
            }
            regis.startuur = startuur;
            regis.einduur = einduur;
            regis.duur = item.get_item('Duur');
            regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
            regis.uitvoerder = auteur;
            regis.ticketOmschrijving = "";
            regis.onsite = item.get_item('Onsite');
            regis.uren = item.get_item('Te_x0020_factureren_x0020_uren') == null ? "0" : item.get_item('Te_x0020_factureren_x0020_uren');
            regis.permanentie = item.get_item('Permanentie');
            regis.typeWerk = item.get_item('Type_x0020_werk');
            regis.status = item.get_item('Status');
            regis.eenheidsprijs = item.get_item('Eenheidsprijs') == null ? "0" : item.get_item('Eenheidsprijs');
            regis.briljant = item.get_item('Aflevernote') == true ? "Ja" : "Nee";
            regis.opmFactuur = item.get_item('Opm_x0020_facturatie') == null ? "" : item.get_item('Opm_x0020_facturatie');

            registraties.push(regis);
            //totaleDuur += Math.round(item.get_item('Duur') * 100) / 100;
        }
        regisIndex++;
        var aantalkeren = Math.ceil(((parseInt(drempelwaardeTijdsregistratie) + 4999) / 5000));
        if (regisIndex < aantalkeren) {
            drempelwaarde += 4999;
            GetTijdsregistraties(datum, drempelwaarde, technieker, ticket, klantnummers, status, aflevernota, index)
        }
        else if ((index + 1) < klantnummers.length) {
            index++;
            drempelwaarde = 0;
            regisIndex = 0;
            GetTijdsregistraties(datum, drempelwaarde, technieker, ticket, klantnummers, status, aflevernota, index)
        }
        else {
            ticketOmschrijvingen = [];
            GetTicketForOmschrijving(klantnummers, registraties, 0);
        }
        //$("#totaalDuurLabel").text(Math.round(totaleDuur * 100) / 100);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de tijdsregistraties niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


function BindTijdsregistraties() {
    $("#registratiesThead").empty();
    $("#registratiesTbody").empty();
    if (registraties.length > 0) {
        var totaalLijst = 0;
        var totaalticket = 0;
        registraties.sort(SortByTicket);
        var currentticket = "0";
        //var j = 0;
        var head = "<tr><th>Ticket</th><th style='display:none;'>ID</th><th>Ticket omschrijving</th><th>Locatie</th><th>Startuur</th><th>Einduur</th><th>Duur</th><th>Facturatie tekst</th>";
        head += "<th>Onsite</th><th>Type werk</th><th>Permanentie</th><th>Status</th><th>Gemaakt door</th><th>Eenheidsprijs</th><th>Facturatieuren</th>";
        head += "<th>Doorgestuurd naar briljant</th><th>Opm factuur</th></tr>";
        $("#registratiesThead").append(head);
        for (var i = 0; i < registraties.length; i++) {
            var tablerow = "";
            if (registraties[i].ticketID !== currentticket) {
                if (i > 0)
                    tablerow += "<tr><td colspan='16' style='padding:6px; border-bottom:1px solid gray; margin: 3px; color: #0171C5; font-size: 1.2em;'>Totaal uren ticket: " + Math.round(totaalticket * 100) / 100 + "</td></tr>"
                tablerow += "<tr><td colspan='16' style='padding:6px; margin: 3px; color: #0171C5; font-size: 1.2em;'>" + registraties[i].ticketID + "</td></tr>";
                totaalticket = 0;
                currentticket = registraties[i].ticketID;
            }
            tablerow += "<tr><td style='display:none;'>" + registraties[i].ticketID + "</td><td style='padding:6px; margin: 3px;'>" + registraties[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].ticketOmschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Locatie + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].startuur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].einduur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].facturatieTekst + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].onsite + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].typeWerk + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].permanentie + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].uitvoerder + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].eenheidsprijs + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].uren + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].briljant + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].opmFactuur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' title='Open tijdsregistratie'><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            tablerow += "</tr>";//"<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenWerkbon(" + registraties[i].ID + ");'><img src='../Images/print.gif' alt='Open werkbon' width='20' /></a></td></tr>";

            $("#registratiesTbody").append(tablerow);
            totaalLijst += Math.round(registraties[i].duur * 100) / 100;
            totaalticket += Math.round(registraties[i].duur * 100) / 100;

        }
        $("#registratiesTbody").append("<tr><td colspan='16' style='padding:6px; border-bottom:1px solid gray; margin: 3px; color: #0171C5; font-size: 1.2em;'>Totaal uren ticket: " + Math.round(totaalticket * 100) / 100 + "</td></tr>");
        $("#registratiesTbody").append("<tr><td colspan='16' style='padding:6px; margin: 3px; color: #0171C5; font-size: 1.2em;'>Totaal uren lijst: " + Math.round(totaalLijst * 100) / 100 + "</td></tr>");

        $("#registratiesTbody > tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}

function SortByTicket(a, b) {
    var aTicket = parseInt(a.ticketID);
    var bTicket = parseInt(b.ticketID);
    return ((aTicket < bTicket) ? -1 : ((aTicket > bTicket) ? 1 : 0));
}

function OpenRegistratie(id) {
    if (id !== "") {
        var url = 'https://itcbe.sharepoint.com/Lists/Tijdsregistratie ticket/Dispform.aspx?ID=' + id;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}

function GetTicketForOmschrijving(Klantnummers, registraties, index) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + Klantnummers[index] + "</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "</ViewFields>"
                        + "<RowLimit>4000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        //var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            ticketOmschrijvingen.push(
            {
                ID: item.get_item('ID'),
                Omschrijving: item.get_item('Title')
            });
        }

        if (index + 1 < globalLocaties.length) {
            index++;
            GetTicketForOmschrijving(Klantnummers, registraties, index);
        }
        else
            SetRegistraties(ticketOmschrijvingen, registraties);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file tickets: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SetRegistraties(arraylist, registraties) {
    for (var i = 0; i < registraties.length; i++) {
        for (var j = 0; j < arraylist.length; j++) {
            if (registraties[i].ticketID == arraylist[j].ID) {
                registraties[i].ticketOmschrijving = arraylist[j].Omschrijving;
            }
        }
    }

    BindTijdsregistraties(registraties);
}


