﻿/*
/* Klanten dropdown elementen 
*/

function GetKlanten() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
            + "<Query>"
                + "<Where>"
                    + "<Neq><FieldRef Name='Archief' /><Value Type='Boolean'>1</Value></Neq>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='Naam1' /><FieldRef Name='Locatie' /></OrderBy> "
            + "</Query>"
          + "<ViewFields>"
             + "<FieldRef Name='Volledige_x0020_klantennaam' />"
             + "<FieldRef Name='ID' />"
             + "<FieldRef Name='Locatie' />"
          + "</ViewFields>"
          + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Volledige_x0020_klantennaam')
            });
        }
        BindKlanten(arraylist);
        GetTechniekers();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlanten(arraylist) {
    $("#klantenSelect").empty();
    if (arraylist.length > 0) {
        arraylist.sort(SortByName);
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Naam + "</option>";
            $("#klantenSelect").append(option);
            $("#ContactKlantSelect").append(option);
            $("#ContactKlantOudSelect").append(option);
            $("#NieuwTicketKlantSelect").append(option);
        }
    }
    CacheItems();
}

function SortByName(a, b) {
    var aName = a.Naam.toLowerCase();
    var bName = b.Naam.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}


/******************************  Klant details ***************************/

function GetKlantenByID(klantid) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
            + "<Query>"
            + "<Where>"
            + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantid + "</Value></Eq>"
            + "</Where>"
            + "</Query>"
              + "<ViewFields>"
                  + "<FieldRef Name='ID' />"
                  + "<FieldRef Name='Naam1' />"
                  + "<FieldRef Name='Title' />"
                  + "<FieldRef Name='Opmerking_x0020_klant' />"
                  + "<FieldRef Name='Opmerking_x0020_locatie' />"
                  + "<FieldRef Name='Opmerkingen_x0020_ivm_x0020_fact' />"
                  + "<FieldRef Name='Postcode' />"
                  + "<FieldRef Name='Locatie' />"
                  + "<FieldRef Name='Straat_x0020__x002b__x0020_numme' />"
                  + "<FieldRef Name='Technieker' />"
                  + "<FieldRef Name='Telefoon' />"
                  + "<FieldRef Name='Vertegenwoordiger' />"
                  + "<FieldRef Name='Groep' />"
                  + "<FieldRef Name='Hosting' />"
              + "</ViewFields>"
              + "<RowLimit>1</RowLimit>"
              + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var arraylist = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();

                arraylist.push(
                {
                    ID: item.get_item('ID'),
                    Bedrijf: item.get_item('Naam1'),
                    Nummer: item.get_item('Title'),
                    Adres: item.get_item('Straat_x0020__x002b__x0020_numme') + " " + item.get_item('Postcode') + " " + item.get_item('Locatie'),
                    Telefoon: item.get_item('Telefoon'),
                    Hosting: item.get_item('Hosting') == true ? "Ja" : "Nee",
                    Vertegenwoordiger: item.get_item('Vertegenwoordiger'),
                    Groep: item.get_item('Groep') == null ? "" : item.get_item('Groep'),
                    Technieker: item.get_item('Technieker'),
                });
            }
            BindKlantDetails(arraylist);
            var onderhoud = GetVolgendeOnderhoudVoorKlant(klantid)
            onderhoud.done(function () {
                //GetContacten(klantid);
            });
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de klant niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function BindKlantDetails(arraylist) {
    if (arraylist.length > 0) {
        $("#IDCell").html(arraylist[0].Nummer);
        $("#bedrijfCell").html(arraylist[0].Bedrijf);
        //$("#facturatieCell").html(arraylist[0].OpmFacturatie);
        //$("#opmklantCell").html(arraylist[0].OpmKlant);
        $("#adresCell").html(arraylist[0].Adres);
        $("#telefoonCell").html(arraylist[0].Telefoon);
        $("#hostingCell").html(arraylist[0].Hosting);
        $("#vertegenwoordigertCell").html(arraylist[0].Vertegenwoordiger);
        //$("#opmLocatieCell").html(arraylist[0].OpmLocatie);
        $("#groepCell").html(arraylist[0].Groep);
        $("#techniekerCell").html(arraylist[0].Technieker);
    }
    else {
        $("#IDCell").html("");
        $("#bedrijfCell").html("");
        //$("#facturatieCell").html("");
        //$("#opmklantCell").html("");
        $("#adresCell").html("");
        $("#telefoonCell").html("");
        $("#hostingCell").html("");
        $("#vertegenwoordigertCell").html("");
        //$("#opmLocatieCell").html("");
        $("#groepCell").html("");
        $("#techniekerCell").html("");
    }
}

/************************ Klanten van dezelfde groep  *******************************/
function GetKlantenVanGroep(klantnummer, groep) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

        var caml = "<View>";
        caml += "<Query>";
        caml += "<Where>";
        if (!groep || groep !== "")
            caml += "<Eq><FieldRef Name='Groep' /><Value Type='Choice'>" + groep + "</Value></Eq>";
        else
            caml += "<Eq><FieldRef Name='Title' /><Value Type='Text'>" + klantnummer + "</Value></Eq>";
        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Naam1' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='ID' />";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Naam1' />";
        caml += "<FieldRef Name='Locatie' />";
        caml += "<FieldRef Name='Groep' />";
        caml += "</ViewFields>";
        caml += "</View>";
        

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Nummer: item.get_item('Title'),
                Naam: item.get_item('Naam1'),
                Locatie: item.get_item('Locatie'),
                Groep: item.get_item('Groep')
            });
        }
        BindGroup(arraylist);
        var klantid = $('#klantenSelect').val();
        
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindGroup(arraylist) {
    $("#GroepDiv").empty();
    if (arraylist.length > 0) {

        var table = "<table><thead><tr><th>ID</th><th>Nummer</th><th>Naam</th><th>Locatie</th></tr></thead><tbody id='groepTbody'>";
        for (var i = 0; i < arraylist.length; i++) {
            table += "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Nummer + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Naam + "</td>";
            table += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Locatie + "</td>";
            table += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='OpenGroepKlant(" + arraylist[i].ID + ")'><img src='../Images/pen_paper_2-512.png' style='width:18px;'/></a></td></tr>";
        }
        table += "</tbody></table>";
        $("#GroepDiv").append(table);
    }
    else
        $("#GroepDiv").append("<p>Geen andere leden in deze groep.</p>");
}

function OpenGroepKlant(id) {
    var url = 'https://itcbe.sharepoint.com/Lists/Klantenlijst1/Editform.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

/************************ nieuwe Klant opslaan *********************/

function IsKlantValid(klant) {
    var valid = true;
    var errortext = "<ul>";
    if (klant.Nummer === "") {
        valid = false;
        errortext += "<li>Geen nummer ingevult!</li>";
    }
    if (klant.bedrijf === "") {
        valid = false;
        errortext += "<li>Geen bedrijfsnaam ingevult!</li>";
    }
    if (klant.Adres === "") {
        valid = false;
        errortext += "<li>Geen adres ingevult!</li>";
    }
    if (klant.Postcode === "") {
        valid = false;
        errortext += "<li>Geen postcode ingevult!</li>";
    } if (klant.Locatie === "") {
        valid = false;
        errortext += "<li>Geen locatie ingevult!</li>";
    }
    if (klant.Vertegenwoordiger === "") {
        valid = false;
        errortext += "<li>Geen vertegenwoordiger ingevult!</li>";
    }
    if (klant.Service === "") {
        valid = false;
        errortext += "<li>Geen managed service ingevult!</li>";
    }
    errortext += "</ul>";
    if (!valid)
        alert(errortext);
    return valid;
}

function GetKlantFromForm() {
    var nummer = $("#NieuweKlantNummertxt").val();
    var bedrijf = $("#NieuweKlantBedrijftxt").val();
    var adres = $("#NieuweKlantAdrestxt").val();
    var postcode = $("#NieuweKlantPostcodetxt").val();
    var locatie = $("#NieuweKlantLocatietxt").val();
    var telefoon = $("#NieuweKlantTelefoontxt").val();
    var fax = $("#NieuweKlantFaxtxt").val();
    var vertegenwoordiger = $("#NieuweKlantVertegenwioordigerSelect").val();
    var sector = $("#NieuweKlantSectorSelect").val();
    var service = $("#NieuweKlantServiceSelect").val();
    var opmerkingfac= $("#NieuweKlantOpmFactuurTextArea").val();
    var hosting = $("#NieuweKlantHostingCheckBox").is(":checked")  == true? "1": "0";
    var opmerkingklant = $("#NieuweKlantOpmerkingKlanttxt").val();
    var technieker = $("#NieuweKlantTechniekerTextArea").val();
    var opmerkinglocatie = $("#NieuweKlantOpmLocatieTextArea").val();
    var typefacturatie = $("#typeFacturatieSelect > option:selected").text();
    var groep = $("#groepSelect > option:selected").text();
    var hoofdzetel = $("#hoofdzetelCheckBox").is(":checked") == true ? "1" : "0";
    var ticketbehandeling = $("#ticketbehandelingSelect > option:selected").val();

    var klant = {
        Nummer: nummer,
        Bedrijf: bedrijf,
        Adres: adres,
        Postcode: postcode,
        Locatie: locatie,
        Telefoon: telefoon,
        Fax: fax,
        Vertegenwoordiger: vertegenwoordiger,
        Sector: sector,
        Service: service,
        OpmerkingFactuur: opmerkingfac,
        TypeFacturatie: typefacturatie,
        Hosting: hosting,
        OpmerkingKlant: opmerkingklant,
        Technieker: technieker,
        OpmerkingLocatie: opmerkinglocatie,
        Groep: groep,
        Hoofdzetel: hoofdzetel,
        Ticketbehandeling: ticketbehandeling
    };

    if (IsKlantValid(klant)) {
        SaveKlant(klant);
    }
}

function SaveKlant(klant) {
    var self = this;
    self.SaveKlant = function () {
        if (klant) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);

            item.set_item("Title", klant.Nummer);
            item.set_item("Naam1", klant.Bedrijf);
            item.set_item("Straat_x0020__x002b__x0020_numme", klant.Adres);
            item.set_item("Postcode", klant.Postcode);
            item.set_item("Locatie", klant.Locatie);
            item.set_item("Telefoon", klant.Telefoon);
            item.set_item("Fax", klant.Fax);
            item.set_item("Vertegenwoordiger", klant.Vertegenwoordiger);
            item.set_item("Sector", klant.Sector);
            item.set_item("Managed_x0020_Services", klant.Service);
            item.set_item("Opmerkingen_x0020_ivm_x0020_fact", klant.OpmerkingFactuur);
            item.set_item("Hosting", klant.Hosting);
            item.set_item("Opmerking_x0020_klant", klant.OpmerkingKlant);
            item.set_item("Technieker", klant.Technieker);
            item.set_item("Opmerking_x0020_locatie", klant.OpmerkingLocatie);
            item.set_item("Type_x0020_facturatie", klant.TypeFacturatie);
            item.set_item("Groep", klant.Groep);
            item.set_item("Hoofdzetel", klant.Hoofdzetel);
            item.set_item("Ticketbehandeling", klant.Ticketbehandeling);

            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Klant opgeslagen.");
        GetKlanten();
        CloseNieuweKlantPopup();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van de Klant: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveKlant();
}

//******************************** TicketBehandeling ophalen ************************************************/

function GetTicketbehandelingen() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticketbehandeling');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                          + "<ViewFields>" 
                            + "<FieldRef Name='ID' />"
                            + "<FieldRef Name='Title' />"
                          + "</ViewFields>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Behandeling: item.get_item('Title')
            });
        }
        BindData(arraylist);
        $(".overlay").addClass("Hidden");
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de ticketbehandelingen niet ophalen!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindData(arraylist) {
    $("#ticketbehandelingSelect").empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            $("#ticketbehandelingSelect").append("<option value='" + arraylist[i].ID + "'>" + arraylist[i].Behandeling + "</option>");
        }
    }
}

function SaveNieuwePCToKlant(klantid, tekst) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');
        var item = list.getItemById(klantid);

        item.set_item('Opmerkingen_x0020_ivm_x0020_fact', tekst);
        item.update();
        context.executeQueryAsync(
            function (args) {
                dfd.resolve();
            },
            function (args) {
                alert('Fout bij het schrijven van de facturatietekst: ' + args.get_message() + '\n' + args.get_stackTrace());
                dfd.reject(args);
            });
    });
    return dfd.promise();
}