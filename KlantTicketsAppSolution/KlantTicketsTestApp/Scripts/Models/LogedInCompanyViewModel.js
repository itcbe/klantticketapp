﻿function GetLogedInCompany(email) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('KlantAccessLijst');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"  
                        + "<Query>"
                        + "<Where><Eq><FieldRef Name='Gebruikersnaam' /><Value Type='Text'>" + email + "</Value></Eq></Where>"
                        + "</Query>" 
                        + "<ViewFields><FieldRef Name='Title' /><FieldRef Name='Klant' /><FieldRef Name='Gebruikersnaam' /><FieldRef Name='MagPrioriteit' /></ViewFields>"
                        + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var klanten = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                var klantvalue = item.get_item('Klant');
                var klantid = klantvalue.get_lookupId();
                var klantnaam = klantvalue.get_lookupValue();
                klanten.push({
                    Naam: klantnaam,
                    ID: klantid,
                    MagPrioriteit: item.get_item('MagPrioriteit')
                });
                UserBedrijf = klantvalue.get_lookupId();
            }
            Bindklanten(klanten);
            UserBedrijf = klanten[0].ID;
            boolPrio = klanten[0].MagPrioriteit;

            if (!boolPrio)
                $('#prioriteitFieldSet').hide();
            else
                $('#prioriteitFieldSet').show();

            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan het bedrijf niet ophalen: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function Bindklanten(klanten) {
    $('#KlantenSelect').empty();
    if (klanten.length > 0) {
        for (var i = 0; i < klanten.length; i++) {
            var option = "<option value='" + klanten[i].ID + "'>" + klanten[i].Naam + "</option>";
            $('#KlantenSelect').append(option);
        }
    }
}