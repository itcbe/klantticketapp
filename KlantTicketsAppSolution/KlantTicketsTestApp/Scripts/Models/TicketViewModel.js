﻿// Globale item variabele om ticket_Id van een nieuw ticket te kunnen inlezen
var item = null;
var opslaanlog;

function GetTicketsByKlant(klantid) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                        + "</And>"
                        //+ "<Geq><FieldRef Name='Opvolgingsdatum' /><Value Type='DateTime'><Today/></Value></Geq>"
                        //+ "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Probleem_x0020_omschrijving' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "<FieldRef Name='Attachments' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Prioriteit' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var arraylist = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                var klant = item.get_item('Klant').get_lookupValue();
                var myklantid = item.get_item('Klant').get_lookupId();
                var contact = item.get_item('Contact').get_lookupValue().split('-')[0];
                var toegewezen = item.get_item('Toegewezen_x0020_aan');
                var usertext = "";
                for (var i = 0; i < toegewezen.length; i++) {
                    var userName = toegewezen[i].get_lookupValue();
                    usertext = usertext + userName + '; ';
                }
                var gewijzigddoor = item.get_item('Editor').get_lookupValue();
                var gewijzigddate = item.get_item('Modified').toString();
                var gewijzigd = ToDateString(gewijzigddate, false);
                var opvolgdate = item.get_item('Opvolgingsdatum').toString();
                var opvolg = ToDateString(opvolgdate, false).split(' ')[0];
                var prioriteit = item.get_item('Prioriteit');
                var bijlagen = item.get_item('Attachments');
                prioriteit = prioriteit.substr(0, 1);
                arraylist.push(
                {
                    Type: item.get_item('Type_x0020_ticket'),
                    ID: item.get_item('ID'),
                    Klant: klant,
                    KlantId: myklantid,
                    Omschrijving: item.get_item('Title'),
                    Contact: contact,
                    Prioriteit: prioriteit,
                    ToegewezenAan: usertext,
                    Probleem: item.get_item('Probleem_x0020_omschrijving') == null ? "" : item.get_item('Probleem_x0020_omschrijving'),
                    Status: item.get_item('Status'),
                    Opvolgdatum: opvolg,
                    Werkgebied: item.get_item('Werkgebied'),
                    Afdeling: item.get_item('Afdeling'),
                    Gewijzigd: gewijzigd,
                    GewijzigdDoor: gewijzigddoor,
                    Bijlagen: bijlagen
                });
            }
            BindKlantTickets(arraylist);
            if (firstTime) {
                $("#ticketTable").tablesorter({
                    sortList: [[2, 1]]
                });
                $("#ticketTable").trigger("updateAll");
                firstTime = false;
            }
            else
                $("#ticketTable").trigger("updateAll");
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de tickets niet ophalen: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function BindKlantTickets(arraylist) {
    $("#ticketTbody").empty();
    $('#interventieTBody').empty();
    //// Zorg er eerst voor dat het be.itc.sp.planning.appointment.js geladen is ...
    //var script = be.itc.sp.getAppWebUrl() + "/Scripts/be.itc.sp.planning.appointment.js";
    //$.getScript(script, function () {
    $('#ticketThead').empty();
    $('#ticketThead').append("<tr>"
                        + "<th>Nummer</th>"
                        + "<th>Omschrijving</th>"
                        + "<th>Probleem</th>"
                        //+ "<th>Prioriteit</th>"
                        + "<th>Toegewezen aan</th>"
                        + "<th>Contact persoon</th>"
                        + "<th>Opvolgingsdatum</th>"
                        + "<th>Status</th>"
                        + "<th>Werkgebied</th>"
                        + "<th>Afdeling</th>"
                        //+ "<th>Laatst gewijzigd</th>"
                        //+ "<th>Door</th>"
                    + "</tr>");

    if (arraylist.length > 0) {
        var j = 0;
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Probleem + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";

            var vandaag = new Date();
            var opvolg = new Date(arraylist[i].Opvolgdatum);
            if (vandaag > opvolg)
                tablerow += "<td style='padding:6px; margin: 3px;'>" + ToNormalDate(vandaag) + "</td>";
            else
                tablerow += "<td style='padding:6px; margin: 3px;'>" + ToNormalDate(opvolg) + "</td>";

            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Afdeling + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;;'>" + arraylist[i].Gewijzigd + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].GewijzigdDoor + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return NieuwAppointment(" + arraylist[i].ID + ");' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px; display: none;'><a href='#' onclick='be.itc.sp.planning.appointment.openAppointmentDialog(" + arraylist[i].ID + ")' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return CopyLink(" + arraylist[i].ID + " );' class='standaardImageButton2' title='Enkel in Internet Explorer'><img src='../Images/Klembord.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");'  class='standaardImageButton2'title='Open ticket'><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return BewerkTicket(" + arraylist[i].ID + ");'  class='standaardImageButton2'title='Bewerk ticket'><img src='../Images/Edit.png' alt='Bewerk ticket' width='18' /></a></td>";
            if (arraylist[i].Bijlagen) {
                tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/paperclip.png' alt='Bijlage aanwezig' title='Bijlagen aanwezig' onclick='ShowBijlagePopup(" + arraylist[i].ID + ")' width='18' /></td>";
            } else {
                tablerow += "<td style='padding:6px; margin: 3px;'></td>";
            }

            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><img src='../Images/clock.png' alt='Bekijk de uitgevoerde taken' title='Bekijk de uitgevoerde werken' width='20' onclick='GetRegistratieOpTicket(" + arraylist[i].ID + ");' /></td>"
            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><img src='../Images/Edit.png' alt='Voeg opmerking toe' title='Voeg opmerking toe' width='20' onclick='ShowWijzigTicketPopup(event, " + arraylist[i].ID + ");' /></td>"
            tablerow += "</tr>";
            //tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td></tr>";

            $("#ticketTbody").append(tablerow);

        }
        $("#ticketTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}

function getCorrectDate(dateStr) {
    var vandaag = new Date();
    var effectieveDatum = new Date(dateStr);
    if (effectieveDatum < vandaag) {
        return ToNormalDate(vandaag, true);
    }
    else {
        return ToNormalDate(effectieveDatum, true);
    }
}

function Groupticket_Click() {
    ResetTicketFilter();
    var group = $("#groepCell").text();
    GetTicketsByGroup(group);
}

function GetTicketsByGroup(Group) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Klant_x003a_Groep_x0020_tekst' /><Value Type='Text'>" + Group + "</Value></Eq>"
                        + "</And>"
                        //+ "<Geq><FieldRef Name='Opvolgingsdatum' /><Value Type='DateTime'><Today/></Value></Geq>"
                        //+ "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Probleem_x0020_omschrijving' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "<FieldRef Name='Attachments' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Klant' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue().split('-')[0];
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false).split(' ')[0];
            var prioriteit = item.get_item('Prioriteit');
            var bijlagen = item.get_item('Attachments');
            prioriteit = prioriteit.substr(0, 1);
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Probleem: item.get_item('Probleem_x0020_omschrijving') == null ? "" : item.get_item('Probleem_x0020_omschrijving'),
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor,
                Bijlagen: bijlagen
            });
        }
        BindGroupTickets(arraylist);

        if (firstTime) {
            $("#ticketTable").tablesorter({
                sortList: [[2, 1]]
            });
            $("#ticketTable").trigger("updateAll");
            firstTime = false;
        }
        else
            $("#ticketTable").trigger("updateAll");
        if (klantChanged)
            GetKlantenByID(myklantid);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindGroupTickets(arraylist) {
    $("#ticketTbody").empty();
    $('#interventieTBody').empty();
    //// Zorg er eerst voor dat het be.itc.sp.planning.appointment.js geladen is ...
    //var script = be.itc.sp.getAppWebUrl() + "/Scripts/be.itc.sp.planning.appointment.js";
    //$.getScript(script, function () {

    $('#ticketThead').empty();
    $('#ticketThead').append("<tr>"
                        + "<th>Nummer</th>"
                        + "<th>Omschrijving</th>"
                        + "<th>Probleem</th>"
                        //+ "<th>Prioriteit</th>"
                        + "<th>Toegewezen aan</th>"
                        + "<th>Contact persoon</th>"
                        + "<th>Opvolgingsdatum</th>"
                        + "<th>Status</th>"
                        + "<th>Werkgebied</th>"
                        + "<th>Afdeling</th>"
                        //+ "<th>Laatst gewijzigd</th>"
                        //+ "<th>Door</th>"
                    + "</tr>");

    if (arraylist.length > 0) {
        var j = 0;
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Probleem + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";

            var vandaag = new Date();
            var opvolg = new Date(arraylist[i].Opvolgdatum);
            if (vandaag > opvolg)
                tablerow += "<td style='padding:6px; margin: 3px;'>" + ToNormalDate(vandaag) + "</td>";
            else
                tablerow += "<td style='padding:6px; margin: 3px;'>" + ToNormalDate(opvolg) + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Afdeling + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;;'>" + arraylist[i].Gewijzigd + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].GewijzigdDoor + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return NieuwAppointment(" + arraylist[i].ID + ");' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px; display: none;'><a href='#' onclick='be.itc.sp.planning.appointment.openAppointmentDialog(" + arraylist[i].ID + ")' ><img src='../Images/Calender.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return CopyLink(" + arraylist[i].ID + " );' class='standaardImageButton2' title='Enkel in Internet Explorer'><img src='../Images/Klembord.gif' alt='Bewerk ticket' width='30' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");'  class='standaardImageButton2'title='Open ticket'><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            //tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return BewerkTicket(" + arraylist[i].ID + ");'  class='standaardImageButton2'title='Bewerk ticket'><img src='../Images/Edit.png' alt='Bewerk ticket' width='18' /></a></td>";
            if (arraylist[i].Bijlagen) {
                tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/paperclip.png' alt='Bijlage aanwezig' title='Bijlagen aanwezig' onclick='ShowBijlagePopup(" + arraylist[i].ID + ")' width='18' /></td>";
            } else {
                tablerow += "<td style='padding:6px; margin: 3px;'></td>";
            }

            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><img src='../Images/clock.png' alt='Bekijk de uitgevoerde taken' title='Bekijk de uitgevoerde werken' width='20' onclick='GetRegistratieOpTicket(" + arraylist[i].ID + ");' /></td>"
            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><img src='../Images/Edit.png' alt='Voeg opmerking toe' title='Voeg opmerking toe' width='20' onclick='ShowWijzigTicketPopup(" + arraylist[i].ID + ");' /></td>"
            tablerow += "</tr>";
            //tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td></tr>";

            $("#ticketTbody").append(tablerow);

        }
        $("#ticketTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}

function RecentGeslotenButton_Click() {
    var dagen = $("#dagentxt").val();
    if (Math.floor(dagen) == dagen && $.isNumeric(dagen)) {
        var currentdate = new Date();
        var zoekdatum = new Date(currentdate);
        zoekdatum.setDate(currentdate.getDate() - dagen);
        var datum = ToSharePointDate(SystemdateToString(zoekdatum));
        GetRecentGesloten(UserBedrijf, datum);
    }
    else {
        alert("De ingegeven dagen is geen geheel getal!");
    }
}

function OpenTicket(ticketID) {
    var url = be.itc.sp.getHostWebUrl() + '/Lists/Ticket/Dispform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

function BewerkTicket(ticketID) {
    var url = be.itc.sp.getHostWebUrl() + '/Lists/Ticket/Editform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

function NieuwAppointment(id) {
    $(".overlay").removeClass('Hidden');
    $("#AgendapuntPopUp").removeClass('Hidden');
}

function HideNieuwAppointmentPopup() {
    $(".overlay").addClass('Hidden');
    $("#AgendapuntPopUp").addClass('Hidden');
}

function FilterTickets() {
    var filtertext = $("#ticketzoektxt").val();
    var tabel = $("#ticketTbody tr");
    if (filtertext == "") {
        tabel.show();
        return;
    }

    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        if ($t.text().toLowerCase().indexOf(filtertext.toLowerCase()) >= 0) {
            return true;
        }
        return false;
    }).show();
}

function GetInitials(users) {
    var userarray = users.split(';');
    var result = "";
    var aantal = userarray.length;
    if (aantal > 0) {
        for (var i = 0; i < aantal - 1; i++) {
            var naam = userarray[i];
            if (naam[0] == ' ')
                naam = naam.substr(1, naam.length);
            var initial = getInitial(naam);
            if ((aantal - 2) == i) {
                result += initial;
            }
            else {
                result += initial + ", ";
            }
        }
    }
    return result;
}

function getInitial(naam) {
    var result = "";
    switch (naam) {
        case "Carlo Daniels":
            result = "CD";
            break;
        case "Davy Craenen":
            result = "DC";
            break;
        case "Davy Janssen":
            result = "DJ";
            break;
        case "Dietmar Braem":
            result = "DB";
            break;
        case "Frank Vanrusselt":
            result = "FV";
            break;
        case "Glenn Vanderborght":
            result = "GV";
            break;
        case "Helpdesk ITC Belgium":
            result = "Helpdesk";
            break;
        case "Inneke Ponet":
            result = "IP";
            break;
        case "Johan Reenaers":
            result = "JR";
            break;
        case "Kristof Dirkx":
            result = "KD";
            break;
        case "Kristof Herten":
            result = "KH";
            break;
        case "Kristof Peters":
            result = "KP";
            break;
        case "Mario Derese":
            result = "MD";
            break;
        case "Mark Meerten":
            result = "MM";
            break;
        case "Martijn  Valkenborgh":
            result = "MV";
            break;
        case "Planning ITC Belgium":
            result = "Planning";
            break;
        case "Ron Stevens":
            result = "RS";
            break;
        case "Sander Verweij":
            result = "SV";
            break;
        case "Stefan Cardinaels":
            result = "SC";
            break;
        case "Stefan Kelchtermans":
            result = "SK";
            break;
    }
    return result;
}

//todo function getInitial

/********************* Opslaan nieuw ticket ***************************/

function IsTicketValid(ticket) {
    var valid = true;
    var errortext = "<ul>";
    if (ticket.ContactID === "0" || ticket.ContactID === "") {
        valid = false;
        errortext += "<li>Geen contact gekozen!</li>";
    }
    if (ticket.Omschrijving === "") {
        valid = false;
        errortext += "<li>Geen Korte omschrijving ingegeven!</li>";
    }
    if (ticket.Omschrijving.length > 40) {
        valid = false;
        errortext += "<li>De korte omschrijving is te lang! max 40 karakters!</li>";
    }
    if (ticket.Werkgebied === "") {
        valid = false;
        errortext += "<li>Geen werkgebied gekozen!</li>";
    }
    if (ticket.ToegewezenAan.length < 1) {
        valid = false;
        errortext += "<li>Geen toegewezen aan gekozen!</li>";
    }
    if (ticket.TypeTicket === "") {
        valid = false;
        errortext += "<li>Geen type ticket gekozen!</li>";
    }
    if (ticket.Afdeling === "") {
        valid = false;
        errortext += "<li>Geen afdeling gekozen!</li>";
    }
    errortext += "</ul>";
    if (!valid) {
        $("#validatieDialog").append(errortext);
        $("#validatieDialog").dialog("open");
        //alert(errortext);
    }
    return valid;
}

function DeleteNieuwTicketData() {
    $("#NieuwTicketOmschrijvingtxt").val("");
    $("#NieuwTicketUrentxt").val("");
    $("#NieuwTicketTakenTextArea").val("");
    $("#NieuwTicketWerkgebiedSelect").val("");
    $('#probleemTxt').val("");
    $('#tecniekerBeschikbaarSelect').val("130");
    $('#prioriteitSelect').val("2 Gemiddeld");
    tinyMCE.get('probleemTxt').setContent("");
}


function GetTicketFromForm() {
    var contactid = $("#NieuwTicketContactSelect").val();
    var omschrijving = $("#NieuwTicketOmschrijvingtxt").val();
    var werkgebied = $("#NieuwTicketWerkgebiedSelect").val();

    var prioriteit = "2 Gemiddeld";

    if (boolPrio) {
        var selectedPrio = $("#prioriteitSelect option:selected");
        prioriteit = selectedPrio.val();
    }

    var toegewezenaan = $("#tecniekerBeschikbaarSelect option:selected");
    var toegewezenLijst = [];
    var id = toegewezenaan.val();
    var naam = toegewezenaan.text();
    var user = { ID: id, Naam: naam };
    toegewezenLijst.push(user);
    var typeticket = "";

    if (naam == "Helpdesk ITC Belgium")
        typeticket = "CALL";
    else
        typeticket = "TASK";
    //if ($("#NieuwTicketCallRadio").is(":checked") == true)
    //    typeticket = "CALL";
    //else if ($("#NieuwTicketTaskRadio").is(":checked") == true) {
    //    typeticket = "TASK";
    //}
    //else {
    //    typeticket = "PROJECT";
    //}

    var status = "";
    if (id === "108") {
        status = "Gepland";
    }
    else {
        status = "Te plannen";
    }
    var vandaag = new Date();
    var opvolgdatum = dateToSharePointDate(vandaag);
    var probleem = CleanText(tinyMCE.get('probleemTxt').getContent({ format: 'raw' }));//$("#probleemTxt").val();
    var afdeling = "Technisch";

    //if ($("#NieuwTickettechnischRadio").is(":checked") === true) {
    //    afdeling = "Technisch";
    //}
    //else if ($("#NieuwTicketSoftwareRadio").is(":checked") === true) {
    //    afdeling = "Software";
    //}
    //else {
    //    afdeling = "Technisch en software";
    //}

    var factureerder = factureerder = "ITC Belgium";

    var ticket = {
        KlantID: UserBedrijf,
        ContactID: contactid,
        Omschrijving: omschrijving,
        Probleem: probleem,
        Werkgebied: werkgebied,
        Prioriteit: prioriteit,
        ToegewezenAan: toegewezenLijst,
        TypeTicket: typeticket,
        Status: status,
        Opvolgdatum: opvolgdatum,
        Taken: probleem,
        Afdeling: afdeling,
        Factureerder: factureerder
    };

    if (IsTicketValid(ticket)) {
        SaveTicket(ticket);
    }
}

function CleanText(werken) {
    //var html = /(<([^>]+)>)/ig;
    //werken = werken.replace(html, '');
    var lt = new RegExp('&lt;', 'g');
    werken = werken.replace(gt, "&#62;");
    var gt = new RegExp('&gt;', 'g');
    werken = werken.replace(lt, "&#60;");
    var find = '&amp;';
    var re = new RegExp(find, 'g');
    werken = werken.replace(re, '&#38;');
    //var result = '<p>' + werken + '</p>';
    return werken;
}

function CleanOmschrijving(s) {
    var temptext = s;

    temptext = temptext.replace(/\&/g, '&#38;');
    temptext = temptext.replace(/</g, "&#60;");
    temptext = temptext.replace(/>/g, "&#62;");
    temptext = temptext.replace(/@/g, "&#64;");

    var arrayTaken = temptext.split('\n');
    var result = "<br/>";
    for (var i = 0; i < arrayTaken.length; i++) {
        result += arrayTaken[i] + "<br/>";
    }
    return result;
}

function SaveChanges(id) {
    var ticket = GetTicketWijzigingen(id);
    SaveTicketChanges(ticket);
}

function GetTicketWijzigingen(id) {
    var contact = $('#WijzigTicketContactSelect').val();
    var opmerking = CleanText(tinyMCE.get('WijzidOpmerkingTextArea').getContent({ format: 'raw' }));//CleanOmschrijving($('#WijzidOpmerkingTextArea').val());
    var toegewezenaan = [];
    var status = "";
    if ($('#TeplannenRadio').is(":checked")){
        toegewezenaan.push({ ID: "130", Naam: "Planning ITC Belgium" });
        status = "Te plannen";
    }
    else if ($('#HelpdeskRadio').is(":checked")){
        toegewezenaan.push({ ID: "108", Naam: "Helpdesk ITC Belgium" });
        status = 'Gepland';
    }

    var ticket = {
        ID: id,
        ContactID: contact,
        Opmerking: opmerking,
        ToegewezenAan: toegewezenaan,
        Status: status
    }
    return ticket;
}

function SaveTicketChanges(ticket) {
    var dfd = $.Deferred(function () {
        if (ticket) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
            var item = list.getItemById(ticket.ID);


            if (ticket.ContactID !== "0")
                item.set_item("Contact", ticket.ContactID);

            if (ticket.Opmerking)
                item.set_item("Uitgevoerde_x0020_taken", ticket.Opmerking);

            if (ticket.ToegewezenAan.length > 0) {
                var toegewezenaan = "";
                for (var i = 0; i < ticket.ToegewezenAan.length; i++) {
                    if (i === 0)
                        toegewezenaan += ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
                    else
                        toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
                }
                item.set_item("Toegewezen_x0020_aan", toegewezenaan);
                if (ticket.Status !== "")
                    item.set_item("Status", ticket.Status);
            }
            item.update();
            context.load(item);
            context.executeQueryAsync(
                function () {
                    alert("Opmerkingen opgeslagen");
                    $("#TicketChangePopup").dialog('close');
                    if (be.itc.sp.rest.fileupload.HasFiles()) {
                        var savebijlage = SaveBijlage(ticket.ID);
                        savebijlage.done(function () {
                            LoadData();
                            ClearWijzigTicket();
                            dfd.resolve();
                        });
                        savebijlage.fail(function () {
                            alert("Bijlage is niet opgeslagen!");
                        });
                    }
                    else {
                        LoadData();
                        ClearWijzigTicket();
                        dfd.resolve();
                    }
                },
                function (sender, args) {
                    alert("Fout bij het opslaan!\n" + args.get_message())
                    dfd.reject();
                });
        }
    });
    return dfd.promise();
}

function SaveBijlage(id) {
    var dfd = $.Deferred(function () {
        be.itc.sp.rest.fileupload.TicketNumber = id;
        // Bijlagen
        if (be.itc.sp.rest.fileupload.HasFiles()) { // niveau 1 bijlagen opslaan
            //Opslaan bijlagen
            var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
            uploadFiles.done(function (data) {
                //opslaanlog += "\nBijlagen opgeslagen";
                //$("#IOSpinnerTitle").html("Controle bijlagen...");
                be.itc.sp.rest.fileupload.ClearFileMap();
                // Controle opgeslagen bijlagen
                var getFiles = be.itc.sp.rest.fileupload.getAttachments(id);
                getFiles.done(function () {
                    if (be.itc.sp.rest.fileupload.HasFiles())
                        be.itc.sp.rest.fileupload.ClearFileMap();
                    dfd.resolve();
                    //spinner.remove();
                    //alert(opslaanlog);
                    //$('#UploadBijlagePopup').dialog('close');
                });
                getFiles.fail(function () {
                    be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het controleren van de bijlagen.");
                    dfd.reject();
                });
            });
            uploadFiles.fail(function (error) {
                be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van de bijlagen.");
                dfd.reject();
            });
        }
    });
    return dfd.promise();
}

function SaveTicket(ticket) {
    this.facturatieTekst = "";
    this.save = function () {
        if (ticket) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
            var itemCreateInfo = new SP.ListItemCreationInformation();
            item = list.addItem(itemCreateInfo);

            var klantlookupvalue = new SP.FieldLookupValue();
            klantlookupvalue.set_lookupId(ticket.KlantID);
            item.set_item("Klant", klantlookupvalue);

            var toegewezenaan = "";
            for (var i = 0; i < ticket.ToegewezenAan.length; i++) {
                if (i === 0)
                    toegewezenaan += ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
                else
                    toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
            }

            item.set_item("Toegewezen_x0020_aan", toegewezenaan);

            item.set_item("Contact", ticket.ContactID);
            item.set_item("Title", ticket.Omschrijving);
            item.set_item("Werkgebied", ticket.Werkgebied);
            item.set_item("Prioriteit", ticket.Prioriteit);
            item.set_item("Type_x0020_ticket", ticket.TypeTicket);
            item.set_item("Status", ticket.Status);
            item.set_item("Opvolgingsdatum", ticket.Opvolgdatum);
            item.set_item("Probleem_x0020_omschrijving", ticket.Taken);
            //item.set_item("Uitgevoerde_x0020_taken", ticket.Taken);
            item.set_item("Afdeling", ticket.Afdeling);
            item.set_item("Factureerder", ticket.Factureerder);
            item.update();
            context.load(item);
            context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
        }
    }
    this.onQuerySucceeded = function () {
        opslaanlog = "Ticket opgeslagen.";
        var spinner;
        spinner = $("<div id='IOSpinner' class='loadingSpinner'><div class='loadingSpinnerMessage'><h2 id='IOSpinnerTitle' ></h2><img src='../Images/loader_5.gif' alt='spinner' /></div></div>");
        $("body").append(spinner);
        spinner.show();
        $("#IOSpinnerTitle").html("Opslaan ticket...");

        be.itc.sp.rest.fileupload.TicketNumber = item.get_item('ID');
        var ticketid = item.get_item('ID');
        var klantid = $('#klantenSelect').val();
        var nieuwticket = false;
        var vlag = $("#NieuweComputerVlag").val();

        // Bijlagen
        if (be.itc.sp.rest.fileupload.HasFiles()) { // niveau 1 bijlagen opslaan
            //Opslaan bijlagen
            $("#IOSpinnerTitle").html("Bijlagen opslaan...");
            var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
            uploadFiles.done(function (data) {
                opslaanlog += "\nBijlagen opgeslagen";
                be.itc.sp.rest.fileupload.ClearFileMap();
                // Controle opgeslagen bijlagen
                $("#IOSpinnerTitle").html("Controle bijlagen...");
                var getFiles = be.itc.sp.rest.fileupload.getAttachments(ticketid);
                getFiles.done(function () {
                    if (be.itc.sp.rest.fileupload.HasFiles())
                        be.itc.sp.rest.fileupload.ClearFileMap();
                    ClosePopupAndRefreshTickets(spinner, klantid);
                });
                getFiles.fail(function () {
                    be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het ophalen van de bijlagen.");
                    ClosePopupAndRefreshTickets(spinner, klantid);
                });
            });
            uploadFiles.fail(function (error) {
                be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van de bijlagen.");
                ClosePopupAndRefreshTickets(spinner, klantid);
            });
        }
        ClosePopupAndRefreshTickets(spinner, klantid)
    }

    this.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van het Ticket: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    this.save();
}

function ClosePopupAndRefreshTickets(spinner, klantid) {
    GetTicketsByKlant(UserBedrijf);
    spinner.remove();
    CloseTicketPopup();
}

function CloseTicketPopup() {
    $('#NieuwTicketPopUp').dialog('close');
    DeleteNieuwTicketData();
}

function InsertTicket(ticket) {
    //var deferred = $.Deferred();
    var url = String.format("{0}/_api/SP.AppContextSite(@target)/web/lists/getbyTitle('{1}')/items?@target='{2}'",
    getQueryStringParameter("SPAppWebUrl"), list, encodeURIComponent(getQueryStringParameter("SPHostUrl")));

    $.ajax({
        url: url,
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify(ticket),
        headers: {
            "Accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            alert(JSON.parse(data.body).d.results[0].ID);
            //deferred.resolve(data);
        },
        error: function (xhr) {
            //deferred.reject(xhr);
        }
    });
    //return deferred.promise();
}

function SaveTicketOpdrachten(ticketid, spinner, klantid, nieuwticket) {
    $("#IOSpinnerTitle").html("Opslaan opdrachten...");
    for (var k = 0; k < nieuwTicketOpdrachten.length; k++) {
        nieuwTicketOpdrachten[k].Ticket = ticketid;
    }
    // opslaan opdrachten
    var OpdrachtenSave = SlaMultiOpdrachtenOpDefferred();
    OpdrachtenSave.done(function () {
        opslaanlog += "\nOpdracht(en) opgeslagen";
        ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid)
    });
    OpdrachtenSave.fail(function () {
        opslaanlog += "\nOpdracht(en) niet opgeslagen";
        be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van de opdrachten.");
        ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid)
    });
}

function SaveComputerTicket(ticketid, spinner, klantid, ticket, nieuwticket) {
    $("#IOSpinnerTitle").html("Opslaan nieuwe computer ticket...");
    var afspraken = $("#prijsafspraak").val();
    if (afspraken != "") {
        var facturatietekst = $('#facturatieCell').html().replace(/<\/div>/, "<br />");
        var klantFacturatieTekst = facturatietekst + "<br /><br />" + ticketid + "<br />" + afspraken + "</div>";
        var nieuwePCSave = SaveNieuwePCToKlant(ticket.KlantID, klantFacturatieTekst);
        nieuwePCSave.done(function () {
            opslaanlog += "\nPrijsafspraken toegevoegd bij de klant.";
            nieuwticket = true;
            var id = $('#npIdHidden').val();
            var setTrue = SetTicketAangemaaktOpTrue(id);
            setTrue.done(function () {
                opslaanlog += "\nTicket aangemaakt op Ja gezet.";
                // zijn er opdrachten
                if (nieuwTicketOpdrachten.length > 0) {
                    SaveTicketOpdrachten(ticketid, spinner, klantid, nieuwticket);
                }
                else {
                    ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
                }
            });
            setTrue.fail(function () {
                opslaanlog += "\nTicket aangemaakt op Ja zetten mislukt.";
                be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het op true zetten van ticket aangemaakt in de nieuwe pc.");
                ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
            });
        });
        nieuwePCSave.fail(function () {
            opslaanlog += "\nPrijsafspraken niet kunnen toevoegen bij de klant.";
            be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van het nieuwe pc ticket.");
            ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
        });
    }
    else {
        nieuwticket = true;
        var id = $('#npIdHidden').val();
        var setTrue = SetTicketAangemaaktOpTrue(id);
        setTrue.done(function () {
            opslaanlog += "\nTicket aangemaakt op Ja gezet.";
            // zijn er opdrachten
            if (nieuwTicketOpdrachten.length > 0) {
                SaveTicketOpdrachten(ticketid, spinner, klantid, nieuwticket);
            }
            else {
                ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
            }
        });
        setTrue.fail(function () {
            opslaanlog += "\nTicket aangemaakt op Ja zetten mislukt.";
            be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het op true zetten van ticket aangemaakt in de nieuwe pc.");
            ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
        });
    }
}

function SaveDringendTicket(item, ticket, ticketid, spinner, klantid, nieuwticket, vlag) {
    $("#IOSpinnerTitle").html("Opslaan dringend ticket...");
    var dringend = setDringend(item, ticket);
    dringend.done(function () {
        opslaanlog += "\nDringend ticket aangemaakt.";
        TicketIsDringend();
        // is het een ticket voor een nieuwe computer
        if (vlag === "ja") {
            SaveComputerTicket(ticketid, spinner, klantid, ticket, nieuwticket);
        }
        else if (nieuwTicketOpdrachten.length > 0) {
            SaveTicketOpdrachten(ticketid, spinner, klantid, nieuwticket);
        }
        else {
            ClosePopupAndRefreshTickets(spinner, nieuwticket, klantid);
        }
    });
    dringend.fail(function () {
        opslaanlog += "\nDringend ticket niet kunnen aangemaken.";
        be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van het dringend ticket.");
    });
}

function SaveOpdrachtToTicketOmschrijving(id, opdracht) {
    var dfd = $.Deferred(function () {
        if (id) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
            var item = list.getItemById(id);

            var taken = "Opdracht: " + opdracht.Titel + " is toegevoegd.";

            item.set_item("Uitgevoerde_x0020_taken", taken);

            item.update();
            context.load(item);
            //context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
            context.executeQueryAsync(function () {
                dfd.resolve();
            }, function (sender, args) {
                opslaanlog += "\nOpdracht toevoegen mislukt.";
                dfd.reject(args);
            });
        }
    });
    return dfd.promise();
}

function OpenNieuweBijlage(ticketid) {
    $('#PopupFileInput').append(new be.itc.sp.rest.fileupload.getFileInput("popupGetFile"));
    $('#UploadBijlagePopup').dialog({
        modal: true,
        minWidth: 540,
        hide: {
            effect: "fade",
            duration: 500
        },
        show: {
            effect: "fade",
            duration: 500
        },
        buttons: [{
            text: "Opslaan",
            click: function () {
                SaveBijlagen(ticketid);
            }
        },
        {
            text: "Annuleren",
            click: function () {
                $(this).dialog("close");
                ClearPopupFileUpload();
            }
        }
        ]
    });
}

function SaveBijlagen(ticketid) {
    opslaanlog = "Bijlagen opslaan.";
    var spinner;
    spinner = $("<div id='IOSpinner' class='loadingSpinner'><div class='loadingSpinnerMessage'><h2 id='IOSpinnerTitle' ></h2><img src='../Images/loader_5.gif' alt='spinner' /></div></div>");
    $("body").append(spinner);
    spinner.show();
    $("#IOSpinnerTitle").html("Opslaan bijlagen...");

    be.itc.sp.rest.fileupload.TicketNumber = ticketid;
    // Bijlagen
    if (be.itc.sp.rest.fileupload.HasFiles()) { // niveau 1 bijlagen opslaan
        //Opslaan bijlagen
        var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
        uploadFiles.done(function (data) {
            opslaanlog += "\nBijlagen opgeslagen";
            $("#IOSpinnerTitle").html("Controle bijlagen...");
            be.itc.sp.rest.fileupload.ClearFileMap();
            // Controle opgeslagen bijlagen
            var getFiles = be.itc.sp.rest.fileupload.getAttachments(ticketid);
            getFiles.done(function () {
                if (be.itc.sp.rest.fileupload.HasFiles())
                    be.itc.sp.rest.fileupload.ClearFileMap();
                spinner.remove();
                alert(opslaanlog);
                $('#UploadBijlagePopup').dialog('close');
            });
            getFiles.fail(function () {
                be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het controleren van de bijlagen.");
            });
        });
        uploadFiles.fail(function (error) {
            be.itc.sp.exception.onError(error, "Er is iets fout gegaan bij het opslaan van de bijlagen.");
        });
    }
}

function SaveImportTicket(tickets, index) {
    var dfd = $.Deferred(function () {
        var ticket = tickets[index];
        if (ticket) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);

            var klantlookupvalue = new SP.FieldLookupValue();
            klantlookupvalue.set_lookupId(ticket.Klanten);
            item.set_item("Klant", klantlookupvalue);

            var toegewezenaan = "";
            for (var i = 0; i < ticket.ToegewezenAan.length; i++) {
                if (i === 0)
                    toegewezenaan += ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
                else
                    toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
            }

            item.set_item("Toegewezen_x0020_aan", toegewezenaan);

            item.set_item("Contact", ticket.Contact);
            item.set_item("Title", ticket.Omschrijving);
            item.set_item("Werkgebied", ticket.Werkgebied);
            item.set_item("Prioriteit", ticket.Prioriteit);
            item.set_item("Type_x0020_ticket", ticket.TypeTicket);
            item.set_item("Status", ticket.Status);
            item.set_item("Opvolgingsdatum", ticket.Opvolgdatum);
            item.set_item("Voorziene_x0020_uren", ticket.Uren);

            item.set_item("Uitgevoerde_x0020_taken", ticket.Taken);
            item.set_item("Afdeling", ticket.Afdeling);
            item.set_item("Factureerder", ticket.Factureerder);
            item.update();
            context.load(item);
            context.executeQueryAsync(function () {
                index++;
                if (index < tickets.length) {
                    SaveImportTicket(tickets, index)
                }
                else {
                    alert("Ticket(s) opgeslagen.");
                    $('#importTicketsPopup').dialog('close');
                    //$('#importTicketsFileInput').empty();
                    $("#importTicketsFileInput").replaceWith($("#importTicketsFileInput").clone(true));
                    $('#importTicketTable').remove();
                    klantselect_onchange();
                    dfd.resolve();
                }
            }, function (sender, args) {
                alert("Fout bij het opslaan van ticket " + index + ".\n" + args.get_message());
                index++;
                if (index < tickets.length) {
                    SaveImportTicket(tickets, index);
                }
                else {
                    dfd.reject();
                }
            });
        }
    });
    return dfd.promise();
}
