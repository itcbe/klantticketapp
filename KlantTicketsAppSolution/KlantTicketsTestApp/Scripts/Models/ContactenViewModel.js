﻿function Contact() {

}

function GetContactenForNieuwTicket(klantid) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where>"
            + "<And>"
                + "<Eq><FieldRef Name='Klant0' LookupId='True' /><Value Type='LookupMulti'>" + klantid + "</Value></Eq>"
                + "<Neq><FieldRef Name='Oud_x0020_contact' /><Value Type='Boolean'>1</Value></Neq>"
            + "</And>"
            + "</Where>"
            + "<OrderBy><FieldRef Name='Naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
           + "</ViewFields>"
          + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var arraylist = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();

                arraylist.push(
                {
                    ID: item.get_item('ID'),
                    Achternaam: item.get_item('Title'),
                    Voornaam: item.get_item('FirstName'),
                    VolledigeNaam: item.get_item('Naam'),
                    Telefoon: item.get_item('WorkPhone') == null ? "" : item.get_item('WorkPhone'),
                    GSM: item.get_item('CellPhone') == null ? "" : item.get_item('CellPhone'),
                    Email: item.get_item('Email')
                });
            }
            BindContactenToSelect(arraylist);
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de contacten niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function BindContactenToSelect(arraylist) {
    $("#NieuwTicketContactSelect").empty();
    //$("#NieuwTicketContactSelect").append("<option value='892'>Dummy dummy</option>");
    if (arraylist.length > 0) {
        $("#NieuwTicketContactSelect").append("<option value='0'></option>");
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";

            $("#NieuwTicketContactSelect").append(option);
        }
        $("#NieuwTicketContactSelect").prepend("<option value='0'></option>");
    }
}

function LoadContacten() {
    var klantid = $("#NieuwTicketKlantSelect").val();
    GetContactenForNieuwTicket(klantid);
}

function GetContacten(klantid) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where><And>"
                + "<Eq><FieldRef Name='Klant0' LookupId='True' /><Value Type='LookupMulti'>" + klantid + "</Value></Eq>"
                + "<Neq><FieldRef Name='Oud_x0020_contact' /><Value Type='Boolean'>1</Value></Neq>"
            + "</And></Where>"
            + "<OrderBy><FieldRef Name='Naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
           + "</ViewFields>"
          + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var arraylist = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();

                arraylist.push(
                {
                    ID: item.get_item('ID'),
                    Achternaam: item.get_item('Title'),
                    Voornaam: item.get_item('FirstName'),
                    VolledigeNaam: item.get_item('Naam').split('-')[0],
                    Telefoon: item.get_item('WorkPhone') == null ? "" : item.get_item('WorkPhone'),
                    GSM: item.get_item('CellPhone') == null ? "" : item.get_item('CellPhone'),
                    Email: item.get_item('Email')
                });
            }
            BindContacten(arraylist);
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de contacten niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function BindContacten(arraylist) {
    $("#contactTbody").empty();
    $("#NieuwTicketContactSelect").empty();
    $('#WijzigTicketContactSelect').empty();
    $('#NieuwTicketContactSelect').append("<option value='0'></option>");
    $('#WijzigTicketContactSelect').append("<option value='892'>Dummy Dummy </option>");

    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var tableRow = "<tr><td>" + arraylist[i].ID + "</td>";
            tableRow += "<td>" + arraylist[i].VolledigeNaam + "</td>";
            tableRow += "<td>" + arraylist[i].Telefoon + "</td>";
            tableRow += "<td>" + arraylist[i].GSM + "</td>";
            if (arraylist[i].Email != null) {
                var email = GetEmailAdressenFromString(arraylist[i].Email);
                tableRow += "<td>" + email + "</td>";
            }
            else {
                tableRow += "<td></td>";
            }
            tableRow += "<td><a href='#' onclick='BewerkContact(" + arraylist[i].ID + ")' title='Bewerk contact' class='standaardImageButton2'><img src='../Images/Edit.png' alt='Bewerk Contact'/></a></td></tr>";

            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";
            $("#NieuwTicketContactSelect").append(option);
            $('#WijzigTicketContactSelect').append(option);
            $("#contactTbody").append(tableRow);
        }
        $("#contactTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}

function GetFullContact(id) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where>"
                + "<Eq><FieldRef Name='ID'/><Value Type='Counter'>" + id + "</Value></Eq>"
            + "</Where>"
            + "<OrderBy><FieldRef Name='Naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
              + "<FieldRef Name='JobTitle' />"
              + "<FieldRef Name='HomePhone' />"
              + "<FieldRef Name='Company' />"
              + "<FieldRef Name='CompanyPhonetic' />"
              + "<FieldRef Name='WorkFax' />"
              + "<FieldRef Name='WorkAddress' />"
              + "<FieldRef Name='LastNamePhonetic' />"
              + "<FieldRef Name='WorkCity' />"
              + "<FieldRef Name='WorkZip' />"
              + "<FieldRef Name='WorkState' />"
              + "<FieldRef Name='WorkCountry' />"
              + "<FieldRef Name='WebPage' />"
              + "<FieldRef Name='FirstNamePhonetic' />"
              + "<FieldRef Name='Comments' />"
           + "</ViewFields>"
           + "<RowLimit>1</RowLimit>"
          + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                var emailtekst = item.get_item('Email');
                var email = stripHTML(emailtekst);

                var website = item.get_item('WebPage') == null ? "http://" : item.get_item('WebPage').get_url();
                var beschrijving = item.get_item('WebPage') == null ? "" : item.get_item('WebPage').get_description();

                $('#contactIdHidden').val(item.get_item('ID'));
                $("#ContactNaamtxt").val(item.get_item('Title'));
                $("#ContactVoornaamtxt").val(item.get_item('FirstName'));
                $("#ContactFunctietxt").val(item.get_item('JobTitle'));
                $("#ContactTelefoonWerktxt").val(item.get_item('WorkPhone'));
                $("#ConatctMobieltxt").val(item.get_item('CellPhone'));
                $("#ContactEmailTextArea").val(email);
                $("#ContactTelefoonThuistxt").val(item.get_item('HomePhone'));
                $("#ContactBedrijftxt").val(item.get_item('Company'));
                $("#ContactFaxtxt").val(item.get_item('WorkFax'));
                $("#ContactAdresTextArea").val(item.get_item('WorkAddress'));
                $("#ContactPlaatstxt").val(item.get_item('WorkCity'));
                $("#ContactProvincietxt").val(item.get_item('WorkState'));
                $("#ContactPostcodetxt").val(item.get_item('WorkZip'));
                $("#ContactLandtxt").val(item.get_item('WorkCountry'));
                $("#ContactWebpaginatxt").val(website);
                $("#ContactWebpaginaBeschrijvingtxt").val(beschrijving);
                $("#ContactNaamFontxt").val(item.get_item('LastNamePhonetic'));
                $("#ContactVoornaamFontxt").val(item.get_item('FirstNamePhonetic'));
                $("#ContactBedrijfsnaamFontxt").val(item.get_item('CompanyPhonetic'));
            }
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de contacten niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function GetEmailAdressenFromString(tekst) {
    var emails = "";
    var myEmails = tekst.split('>');
    var tempMails = [];
    for (var i = 0; i < myEmails.length; i++) {
        if (myEmails[i].indexOf('@') > -1)
            if (myEmails[i].indexOf("<a href") == -1)
                tempMails.push(myEmails[i]);
    }
    for (var j = 0; j < tempMails.length; j++) {
        var item = tempMails[j];
        if (item.indexOf('<') > -1) {
            item = item.substring(0, (item.indexOf('<')));
        }
        emails += "<a href='mailto:" + item + "'>" + item + "</a>";
        if (j < (tempMails.length - 1))
            emails += "</br>";
    }
    return emails;
}

function stripHTML(dirtyString) {
    var container = document.createElement('div');
    container.innerHTML = dirtyString;
    return container.textContent || container.innerText;
}

function BewerkContact(id) {
    var getFullContact = GetFullContact(id);
    getFullContact.done(function () {
        //$("#NieuwContactPopUp").prop('title', "Bewerk contact");
        $("#NieuwContactPopUp").dialog({
            width: 700,
            modal: true,
            title: "Bewerk contact",
            hide: {
                effect: "fade",
                duration: 500
            },
            show: {
                effect: "fade",
                duration: 500
            },
            maxHeight: 700,
            buttons: [{
                text: "Opslaan",
                click: function () {
                    GetContactFromForm(true);
                }
            },
                {
                    text: "Annuleren",
                    click: function () {
                        $(this).dialog("close");
                        ClearContact();
                    }
                }
            ]
        });

    });
}

function GetAllContacten() {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
          + "<Query>"
            + "<Where>"
                + "<Neq><FieldRef Name='Oud_x0020_contact' /><Value Type='Boolean'>1</Value></Neq>"
            + "</Where>"
            + "<OrderBy><FieldRef Name='Naam' /></OrderBy>"
          + "</Query>"
          + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='FirstName' />"
              + "<FieldRef Name='Naam' />"
              + "<FieldRef Name='WorkPhone' />"
              + "<FieldRef Name='CellPhone' />"
              + "<FieldRef Name='Email' />"
           + "</ViewFields>"
          + "</View>");

        var pendingItems = list.getItems(query);
        clientContext.load(pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = pendingItems.getEnumerator();
            var contacten = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();

                contacten.push(
                {
                    ID: item.get_item('ID'),
                    Achternaam: item.get_item('Title'),
                    Voornaam: item.get_item('FirstName'),
                    VolledigeNaam: item.get_item('Naam'),
                    Telefoon: item.get_item('WorkPhone'),
                    GSM: item.get_item('CellPhone'),
                    Email: item.get_item('Email')
                });
            }
            dfd.resolve(contacten);
        },
        function (sender, args) {
            alert("Fout bij het laden van alle contacten.\n" + args.get_message());
            dfd.reject();
        }
        );
    });
    return dfd.promise();
}


/******************  NIEUW CONTACT OPSLAAN ***********************/

function IsNieuwContactValid(Contact) {
    var valid = true;
    var errortext = "<ul>";
    if (Contact.Naam == "") {
        valid = false;
        errortext += "<li>Achternaam is niet ingevult.</li>"
    }
    if (Contact.Voornaam == "") {
        valid = false;
        errortext += "<li>Voornaam is niet ingevult.</li>"
    }
    errortext += "</ul>";
    if (!valid){
        $("#validatieDialog").append(errortext);
        $("#validatieDialog").dialog("open");
    }
    return valid;
}

function GetContactFromForm(update) {
    var id = "0";
    if (update) {
        id = $('#contactIdHidden').val();
    }
    var naam = $("#ContactNaamtxt").val();
    var voornaam = $("#ContactVoornaamtxt").val();
    var klanten = [];
    var klant = { ID: UserBedrijf};
    klanten.push(klant);
    var functie = $("#ContactFunctietxt").val();
    var email = $("#ContactEmailTextArea").val();
    var mobiel = $("#ConatctMobieltxt").val();
    var tWerk = $("#ContactTelefoonWerktxt").val();
    var tthuis = $("#ContactTelefoonThuistxt").val();
    var bedrijf = $("#ContactBedrijftxt").val();
    var fax = $("#ContactFaxtxt").val();
    var adres = $("#ContactAdresTextArea").val();
    var plaats = $("#ContactPlaatstxt").val();
    var provincie = $("#ContactProvincietxt").val();
    var postcode = $("#ContactPostcodetxt").val();
    var land = $("#ContactLandtxt").val();
    var website = $("#ContactWebpaginatxt").val();
    var beschrijving = $("#ContactWebpaginaBeschrijvingtxt").val();
    //var notities = $("#ContactNotitieTextArea").val();
    var naamfon = $("#ContactNaamFontxt").val();
    var voornaamfon = $("#ContactVoornaamFontxt").val();
    var bedrijffon = $("#ContactBedrijfsnaamFontxt").val();
    //var klantoud = $("#ContactKlantOudSelect").val();
    var oudcontact = $("#uitDienstCheckBox").is(':checked') == true ? "1" : "0";

    var contact = {
        ID: id,
        Naam: naam,
        Voornaam: voornaam,
        Klant: klanten,
        Functie: functie,
        Email: email,
        Mobiel: mobiel,
        TelefoonWerk: tWerk,
        TelefoonThuis: tthuis,
        Bedrijf: bedrijf,
        Fax: fax,
        Adres: adres,
        Plaats: plaats,
        Provincie: provincie,
        Postcode: postcode,
        Land: land,
        Webpagina: website,
        Beschrijving: beschrijving,
        AchternaamFon: naamfon,
        VoornaamFon: voornaamfon,
        BedrijfsnaamFon: bedrijffon,
        UitDienst: oudcontact
    };
    if (IsNieuwContactValid(contact)) {
        if (update) {
            var update = UpdateContact(contact);
            update.done(function () {
                alert("Contact opgeslagen.");
                CloseNieuwContactPopup();
                GetContacten(UserBedrijf);
            });      
        }
        else{
            var save = SaveContact(contact);
            save.done(function () {
                alert("Contact opgeslagen.");
                CloseNieuwContactPopup();
                GetContacten(UserBedrijf);          
            });
        }
    }
}

function SaveContact(contact) {
    var dfd = $.Deferred(function () {
        if (contact) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);
            var klanten = "";
            var klantlookupVals = [];
            for (var i = 0; i < contact.Klant.length; i++) {
                var lookupvalue = new SP.FieldLookupValue();
                lookupvalue.set_lookupId(contact.Klant[i].ID);
                klantlookupVals.push(lookupvalue);
            }

            var weblink = new SP.FieldUrlValue();
            weblink.set_url(contact.Webpagina);
            weblink.set_description(contact.Beschrijving);

            item.set_item("Klant0", klantlookupVals);
            item.set_item("Title", contact.Naam);
            item.set_item("FirstName", contact.Voornaam);
            item.set_item("JobTitle", contact.Functie);
            item.set_item("Email", contact.Email);
            item.set_item("CellPhone", contact.Mobiel);
            item.set_item("WorkPhone", contact.TelefoonWerk);
            item.set_item("HomePhone", contact.TelefoonThuis);
            item.set_item("Company", contact.Bedrijf);
            item.set_item("WorkFax", contact.Fax);
            item.set_item("WorkAddress", contact.Adres);
            item.set_item("WorkCity", contact.Plaats);
            item.set_item("WorkState", contact.Provincie);
            item.set_item("WorkZip", contact.Postcode);
            item.set_item("WorkCountry", contact.Land);
            item.set_item("WebPage", weblink);
            item.set_item("LastNamePhonetic", contact.AchternaamFon);
            item.set_item("FirstNamePhonetic", contact.VoornaamFon);
            item.set_item("CompanyPhonetic", contact.BedrijfsnaamFon);
            item.set_item("Oud_x0020_contact", contact.UitDienst);

            item.update();
            context.executeQueryAsync(function () {
                dfd.resolve();
            }, function (sender, args) {
                alert('Fout bij het opslaan van het contact: ' + args.get_message() + '\n' + args.get_stackTrace());
                dfd.reject();
            });
        }
    });
    return dfd.promise();
}

function UpdateContact(contact) {
    var dfd = $.Deferred(function () {
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        //var itemCreateInfo = new SP.ListItemCreationInformation();
        var item = list.getItemById(contact.ID);
        var klanten = "";
        var klantlookupVals = [];
        for (var i = 0; i < contact.Klant.length; i++) {
            var lookupvalue = new SP.FieldLookupValue();
            lookupvalue.set_lookupId(contact.Klant[i].ID);
            klantlookupVals.push(lookupvalue);
        }
        var weblink = new SP.FieldUrlValue();
        weblink.set_url(contact.Webpagina);
        weblink.set_description(contact.Beschrijving);

        item.set_item("Klant0", klantlookupVals);
        item.set_item("Title", contact.Naam);
        item.set_item("FirstName", contact.Voornaam);
        item.set_item("JobTitle", contact.Functie);
        item.set_item("Email", contact.Email);
        item.set_item("CellPhone", contact.Mobiel);
        item.set_item("WorkPhone", contact.TelefoonWerk);
        item.set_item("HomePhone", contact.TelefoonThuis);
        item.set_item("Company", contact.Bedrijf);
        item.set_item("WorkFax", contact.Fax);
        item.set_item("WorkAddress", contact.Adres);
        item.set_item("WorkCity", contact.Plaats);
        item.set_item("WorkState", contact.Provincie);
        item.set_item("WorkZip", contact.Postcode);
        item.set_item("WorkCountry", contact.Land);
        item.set_item("WebPage", weblink);
        item.set_item("LastNamePhonetic", contact.AchternaamFon);
        item.set_item("FirstNamePhonetic", contact.VoornaamFon);
        item.set_item("CompanyPhonetic", contact.BedrijfsnaamFon);
        item.set_item("Oud_x0020_contact", contact.UitDienst);

        item.update();
        context.executeQueryAsync(function () {
            dfd.resolve();
        }, function (sender, args) {
            alert('Fout bij het opslaan van het contact: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function onSaveContactQuerySucceeded(sender, args) {
    alert("Contact opgeslagen.");
    var klantid = $("#klantenSelect").val();
    GetContacten(klantid);
    CloseNieuwContactPopup();
}

function onSaveContactQueryFailed(sender, args) {
    alert('Fout bij het opslaan van het contact: ' + args.get_message() + '\n' + args.get_stackTrace());
}
