﻿"use strict";
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};

be.itc.sp = (function (sp) {
    var _context = SP.ClientContext.get_current();
    var _currentUser = _context.get_web().get_currentUser();
    var _appweburl;
    var _hostweburl;

    sp.getContext = function () {
        return _context;
    };

    sp.getCurrentUser = function () {
        return _currentUser;
    };

    sp.getQueryStringParameter = function (paramToRetrieve) {
        var params =
        document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve) {
                var url = singleParam[1];
                url = url.replace(/%3A/g, ':');
                url = url.replace(/%2F/g, '/');
                url = url.replace(/%2E/g, '.');
                url = url.replace(/%5F/g, '_');
                return url;
            }
        }
    };

    sp.getAppWebUrl = function () {
        if (_appweburl == undefined) {
            _appweburl = decodeURIComponent(sp.getQueryStringParameter("SPAppWebUrl"));
            if (_appweburl.slice(-1) == "#") {
                _appweburl = _appweburl.slice(0, _appweburl.length - 1);
            }
        }
        return _appweburl;
    }

    sp.getHostWebUrl = function () {
        if (_hostweburl == undefined) {
            _hostweburl = decodeURIComponent(sp.getQueryStringParameter("SPHostUrl"));
            if (_hostweburl.slice(-1) == "#") {
                _hostweburl = _hostweburl.slice(0, _hostweburl.length - 1);
            }
        }
        return _hostweburl;
    }



    return sp;

})(be.itc.sp || {});
