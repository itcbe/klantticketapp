﻿/**************************** Utility's *******************************/

function getQueryStringParameter(paramToRetrieve) {
    var params =
    document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve) {
            var url = singleParam[1];
            url = url.replace('%3A', ':');
            url = url.replace('%2F', '/');
            url = url.replace('%2F', '/');
            url = url.replace('%2E', '.');
            url = url.replace('%2E', '.');
            return url;
        }
    }
}

/* Datum functies */

function ToSharePointDate(date) {
    var dateArray = date.split('/');
    var result = "";
    var day = dateArray[0];
    var month = dateArray[1];
    var yearTime = dateArray[2].split(' ');
    var year = yearTime[0];
    var time = yearTime[1];
    if (time) {
        var timeArray = time.split(":");
        var hour = timeArray[0];
        var min = timeArray[1];
        hour = hour - 1;
        if (hour < 10) {
            hour = "0" + hour;

        }
        result = year + "-" + month + "-" + day + "T" + hour + ":" + min + ":00";
    }
    else
        result = year + "-" + month + "-" + day + "T00:00:00";

    return result;
}

function dateToSharePointDate(date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    return year + "-" + month + "-" + day + "T02:00:00";
}

function StringToDate(strdate) {
    var temp = "";
    var dateArray = strdate.split('/');
    var day = dateArray[0] < 10 ? '0' + dateArray[0] : dateArray[0];
    var month = dateArray[1] < 10 ? '0' + dateArray[1] : dateArray[1];
    var year = dateArray[2];
    temp = month + "/" + day + "/" + year;

    return new Date(temp);
}

function NaarDagMaandJaar(strdate) {
    var temp = "";
    var dateArray = strdate.split('/');
    var dagArray = dateArray[2].split(' ');
    var day = dagArray[0];
    var month = dateArray[1];// < 10 ? '0' + dateArray[1] : dateArray[1];
    var year = dateArray[0];
    temp = day + "/" + month + "/" + year;

    return temp;
}

function IsSafari() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') > -1) {
        if (ua.indexOf('chrome') > -1) {
            return false;
        }
        else
            return true;
    }
    else
        return false;
}

function IsChrome() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('chrome') > -1) {
        return true;
    }
    else
        return false;
}

function GetSafariDate(datum) {
    var dagMaandArray = datum.split(' ');
    var temp = dagMaandArray[2];
    var dag = temp.substr(0, temp.length - 1);
    var maand = "";
    switch (dagMaandArray[1]) {
        case "January":
            maand = "01";
            break;
        case "February":
            maand = "02";
            break;
        case "March":
            maand = "03";
            break;
        case "April":
            maand = "04";
            break;
        case "May":
            maand = "05";
            break;
        case "June":
            maand = "06";
            break;
        case "July":
            maand = "07";
            break;
        case "August":
            maand = "08";
            break;
        case "September":
            maand = "09";
            break;
        case "October":
            maand = "10";
            break;
        case "November":
            maand = "11";
            break;
        case "December":
            maand = "12";
            break;
    }
    var jaar = dagMaandArray[3];
    var result = dag + "/" + maand + "/" + jaar;
    return result;
}

function GetMozillaDate(datum, datepicker) {
    var dagMaandArray = datum.split('-');
    //var jaarUurArray = dagMaandArray[2].split(' ');
    var dag = dagMaandArray[0];
    var maand = dagMaandArray[1] < 10 ? "0" + dagMaandArray[1] : dagMaandArray[1];
    var jaar = dagMaandArray[2];
    var result = dag + "/" + maand + "/" + jaar;
    return result;
}

function SystemdateToString(date) {
    var datum = new Date(date);
    var dag = datum.getDate();
    var maand = datum.getMonth();
    var jaar = datum.getFullYear();

    return dag + "/" + (maand + 1)  + "/" + jaar;
}

function ToNormalDate(datum) {
    var dag = datum.getDate();
    var maand = datum.getMonth() + 1;
    var jaar = datum.getFullYear();
    var value = dag + "/" + (maand < 10 ? ("0" + maand) : maand) + "/" + jaar;
    return value;
}

function ToDateString(datum, datepicker) {
    var dag = "";
    var maand = "";
    var jaar = "";
    var uur = "";
    if (IsChrome() || IsSafari() || $.browser.mozilla) {
        var datumArray = datum.split(' ');
        dag = datumArray[2];
        jaar = datumArray[3];
        maand = "";
        uur = "";
        if (datumArray.length > 4)
            uur = datumArray[4];
        maand = GetMonthNumber(datumArray[1])
    }
    else {
        var datumArray = datum.split(' ');
        dag = datumArray[2];
        jaar = datumArray[datumArray.length - 1];
        maand = "";
        uur = "";
        if (datumArray.length > 3)
            uur = datumArray[3];
        maand = GetMonthNumber(datumArray[1])
    }


    var result = "";
    if (datepicker)
        result += dag + "/" + maand + "/" + jaar;
    else {
        result += jaar + "/" + maand + "/" + dag;
        if (uur !== "")
            result += " " + uur;
    }
    return result;

}

function GetMonthNumber(maand) {
    var result = "";
    switch (maand) {
        case "Jan":
            result = "01";
            break;
        case "Feb":
            result = "02";
            break;
        case "Mar":
            result = "03";
            break;
        case "Apr":
            result = "04";
            break;
        case "May":
            result = "05";
            break;
        case "Jun":
            result = "06";
            break;
        case "Jul":
            result = "07";
            break;
        case "Aug":
            result = "08";
            break;
        case "Sep":
            result = "09";
            break;
        case "Oct":
            result = "10";
            break;
        case "Nov":
            result = "11";
            break;
        case "Dec":
            result = "12";
            break;
    }
    return result;
}

function GetDateString(datum, datepicker) {
    var dag = "";
    var maand = "";
    var jaar = "";
    var uur = "";
    if ($.browser.mozilla) {
        var dagMaandArray = datum.split('-');
        var jaarUurArray = dagMaandArray[2].split(' ');
        dag = dagMaandArray[0];
        maand = dagMaandArray[1] < 10 ? "0" + dagMaandArray[1] : dagMaandArray[1];
        jaar = jaarUurArray[0];
        if (jaarUurArray.length > 1)
            uur = jaarUurArray[1];
    }
    else if (IsSafari()) {
        var dagMaandArray = datum.split(' ');
        var temp = dagMaandArray[2];
        dag = dagMaandArray[0];
        maand = "";
        uur = "";
        switch (dagMaandArray[1]) {
            case "januari":
                maand = "01";
                break;
            case "februari":
                maand = "02";
                break;
            case "maart":
                maand = "03";
                break;
            case "april":
                maand = "04";
                break;
            case "mei":
                maand = "05";
                break;
            case "juni":
                maand = "06";
                break;
            case "july":
                maand = "07";
                break;
            case "augustus":
                maand = "08";
                break;
            case "september":
                maand = "09";
                break;
            case "oktober":
                maand = "10";
                break;
            case "november":
                maand = "11";
                break;
            case "december":
                maand = "12";
                break;
        }
        jaar = dagMaandArray[2];
        if (dagMaandArray.length > 3)
            uur = dagMaandArray[3];
    }
    else if (IsChrome()) {
        var datumArray = datum.split('-');
        dag = datumArray[0];
        maand = datumArray[1] < 10 ? "0" + datumArray[1] : datumArray[1];
        jaar = datumArray[2];
        uur = "";
        if (datumArray.length > 3)
            uur = datumArray[3];


    }
    else {
        var datumArray = datum.split(' ');
        dag = datumArray[1];
        jaar = datumArray[3];
        maand = "";
        uur = "";
        if (datumArray.length > 4)
            uur = datumArray[4];
        switch (datumArray[2]) {
            case "januari":
                maand = "01";
                break;
            case "februari":
                maand = "02";
                break;
            case "maart":
                maand = "03";
                break;
            case "april":
                maand = "04";
                break;
            case "mei":
                maand = "05";
                break;
            case "juni":
                maand = "06";
                break;
            case "juli":
                maand = "07";
                break;
            case "augustus":
                maand = "08";
                break;
            case "september":
                maand = "09";
                break;
            case "oktober":
                maand = "10";
                break;
            case "november":
                maand = "11";
                break;
            case "december":
                maand = "12";
                break;
        }
    }

    var result = "";
    if (datepicker)
        result += dag + "/" + maand + "/" + jaar;
    else
        result += jaar + "/" + maand + "/" + dag;
    if (uur !== "")
        result += " " + uur;
    return result;
}
