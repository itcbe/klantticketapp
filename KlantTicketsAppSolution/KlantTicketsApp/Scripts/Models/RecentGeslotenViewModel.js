﻿function GetRecentGesloten(klantid, datum) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
                + "<Where>"
                    + "<And>"
                        + "<And>"
                            + "<And>"
                                + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                                + "<Eq><FieldRef Name='Status'/><Value Type='Text'>Afgesloten</Value></Eq>"
                            + "</And>"
                            + "<Gt><FieldRef Name='Modified' /><Value Type='DateTime' IncludeTimeValue='TRUE'>" + datum + "</Value></Gt>"
                        + "</And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                    + "</And>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='Modified' Ascending='FALSE' /></OrderBy>"
            + "</Query>"
            + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Probleem_x0020_omschrijving' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "<FieldRef Name='Attachments' />"

            + "</ViewFields>"
         + "</View>");


        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue().split('-')[0];
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false).split(' ')[0];
            var prioriteit = item.get_item('Prioriteit');
            var bijlagen = item.get_item('Attachments');
            prioriteit = prioriteit.substr(0, 1);
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Probleem: item.get_item('Probleem_x0020_omschrijving') == null ? "" : item.get_item('Probleem_x0020_omschrijving'),
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor,
                Bijlagen: bijlagen
            });

        }
        BindRecentGesloten(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan recent gesloten tickets niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindRecentGesloten(arraylist) {
    $("#recentThead").empty();
    $("#recentTbody").empty();
    $('#recentThead').append("<tr>"
                        + "<th>Nummer</th>"
                        + "<th>Omschrijving</th>"
                        + "<th>Probleem</th>"
                        + "<th>Toegewezen aan</th>"
                        + "<th>Contact persoon</th>"
                        + "<th>Opvolgingsdatum</th>"
                        + "<th>Status</th>"
                        + "<th>Werkgebied</th>"
                        + "<th></th>"
                        + "<th></th>"
                        + "<th></th>"
                    + "</tr>");


    if (arraylist.length > 0) {
        var j = 0;
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + arraylist[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Probleem + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + arraylist[i].Afdeling + "</td>";
            if (arraylist[i].Bijlagen) {
                tablerow += "<td style='padding:6px; margin: 3px;'><img src='../Images/paperclip.png' alt='Bijlage aanwezig' title='Bijlagen aanwezig' onclick='ShowBijlagePopup(" + arraylist[i].ID + ")' width='18' /></td>";
            } else {
                tablerow += "<td style='padding:6px; margin: 3px;'></td>";
            }

            tablerow += "<td style='padding:6px; margin: 3px;' class='standaardImageButton2'><img src='../Images/clock.png' alt='Bekijk de tijdsregistraties' title='Bekijk de tijdsregistraties' width='20' onclick='GetRegistratieOpTicket(" + arraylist[i].ID + ")' /></td>"

            tablerow += "</tr>";

            $("#recentTbody").append(tablerow);

        }
        $("#recentTbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}