﻿function GetStatussen() {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
        var fields = list.get_fields();

        var opdrachten = clientContext.castTo(list.get_fields().getByInternalNameOrTitle("Status"), SP.FieldChoice);

        clientContext.load(list);
        clientContext.load(fields);
        clientContext.load(opdrachten);
        clientContext.executeQueryAsync(
            function () {
                var context = new SP.ClientContext.get_current();
                // Converting the Field to SPFieldChoice from the execution results
                var myChoicesfield = context.castTo(fields.getByInternalNameOrTitle("Status"), SP.FieldChoice);
                //get_choices() method will return the array of choices provided in the field

                var choices = myChoicesfield.get_choices();

                var StatusSelect = $("#NieuwTicketStatusSelect");
                StatusSelect.empty();
                if (choices.length > 0) {
                    for (var i = 0; i < choices.length; i++) {
                        StatusSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
                    }
                }
                dfd.resolve();
            },
            function (sender, args) {
                alert('Kan de Statussen niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
                dfd.reject();
            });
    });
    return dfd.promise();
}