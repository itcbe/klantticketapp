﻿GetItemsFromCalendarAsmx = function (webUrl, calendarGuid) {

    wsURL = webUrl + "/_vti_bin/Lists.asmx";

    var xmlCall =
        "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'> <soap:Body>" +
        "<GetListItems xmlns='http://schemas.microsoft.com/sharepoint/soap/'>" +
        "<listName>" + calendarGuid + "</listName>" +
        "<query>" +
        "<Query>" +
        "<Where>" +
           "<DateRangesOverlap>" +
           "<FieldRef Name=\"EventDate\" />" +
           "<FieldRef Name=\"EndDate\" />" +
           "<FieldRef Name=\"RecurrenceID\" />" +
           "<Value Type='DateTime'><Year/></Value>" +
           "</DateRangesOverlap>" +
        "</Where>" +
        "</Query>" +
        "</query>" +
        "<queryOptions>" +
        "<QueryOptions>" +
            "<ExpandRecurrence>TRUE</ExpandRecurrence>" +
        "</QueryOptions>" +
        "</queryOptions>" +
        "</GetListItems>" +
        "</soap:Body></soap:Envelope>";
    var result = [];
    $.ajax({
        url: wsURL,
        type: "POST",
        dataType: "xml",
        async: false,
        data: xmlCall,
        complete: function (xData, status) {
            //Core.LogMessage("Core.GetItemsFromCalendarAsmx-> url: " + wsURL + " status: " + status);
            if (status === "success") {
                var root = $(xData.responseText);
                root.find("listitems").children().children().each(function () {
                    $this = $(this);
                    var ids = $this.attr("ows_UniqueId").split(";");
                    var rec = $this.attr("ows_fRecurrence");
                    result.push({
                        "StartTime": $this.attr("ows_EventDate"),
                        "EndTime": $this.attr("ows_EndDate"),
                        "Title": $this.attr("ows_Title"),
                        "Recurrence": (rec === "1" ? true : false),
                        //"Description": Core.HtmlDecode($this.attr("ows_Description")),
                        "Guid": ids[1],
                        "Id": ids[0],
                    });
                });
            }
        },
        contentType: "text/xml; charset=\"utf-8\""
    });
    return result;
};

function GetVolgendeOnderhoudVoorKlant(klantid) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Onderhoudsagenda');
        var view = list.get_views().getByTitle("Huidige gebeurtenissen");
        clientContext.load(view);

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                + "<Query>"
                + "<Where>"
                + "<And>"
                + "<Eq><<FieldRef Name='Klant_x0020__x002d__x0020_Locati' LookupId='True'/><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                + "<Geq><FieldRef Name='EndDate' /><Value Type='DateTime'><Today /></Value></Geq>"
                + "</And>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='EventDate' /></OrderBy>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='Klant_x0020__x002d__x0020_Locati' />"
                + "<FieldRef Name='EventDate' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Location' />"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='EndDate' />"
                + "<FieldRef Name='Description' />"
                + "</ViewFields>" 
                + "<RowLimit>10</RowLimit>" 
                + "</View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var onderhouden = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                var beginuur = item.get_item('EventDate');
                var einduur = item.get_item('EndDate')
                var titel = item.get_item('Title');
                var omschrijving = item.get_item('Description');
                onderhouden.push({
                    ID: item.get_item('ID'),
                    Klantid: klantid,
                    Beginuur: beginuur,
                    Einduur: einduur,
                    Titel: titel,
                    Omschrijving: omschrijving
                });           
            }
            BindOnderhoud(onderhouden);
            dfd.resolve();
        },
        function (sender, args) {
            alert("Fout bij het ophalen van het volgende onderhoud!" + args.get_message());
            dfd.reject();
        }
        );
    });
    return dfd.promise();
}

function BindOnderhoud(onderhouden) {
    $('#volgendonderhoudCell').text("");
    if (onderhouden.length > 0) {
        var vandaag = new Date();
        var datum = "";
        var info = "";
        for (var i = 0; i < onderhouden.length; i++) {
            var dag = new Date(onderhouden[i].Beginuur);
            if (dag >= vandaag) {
                datum = dag;
                break;
            }
            else
                info = "<a href='#' onclick='OpenOnderHoudsAgenda(" + onderhouden[i].ID + ")'>" + onderhouden[i].Titel + "</a>";
        }
        if (datum !== "") {
            $('#volgendonderhoudCell').text("Volgend onderhoud: " + datum + ".");
        }
        else{
            $('#volgendonderhoudCell').html(info);
        }
    }
}

function GetOnderhoudsInfo() {
    var onderhoud = GetItemsFromCalendarAsmx(hostweburl, "Onderhoudsagenda");
    alert(onderhoud);
}

function OpenOnderHoudsAgenda(id) {
    //https://itcbe.sharepoint.com/Lists/Onderhoudsagenda%20ZVB/calendar.aspx?ID=23
    var url = be.itc.sp.getHostWebUrl() + '/Lists/Onderhoudsagenda%20ZVB/calendar.aspx?ID=' + id;///Dispform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}