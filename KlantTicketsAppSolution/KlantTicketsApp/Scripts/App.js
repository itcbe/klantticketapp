﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var UserEmail, UserBedrijf;
var firstTime = true, first = true, klantChanged = false;
var ddlText, ddlValue, ddl, lblMesg, zoektekst = "";
var drempelwaardeTicket, drempelwaardeTijdsregistratie;
var appweburl, hostweburl;
var nieuwTicketOpdrachten = [];
var boolPrio = false;

// jQuery 1.9 is out… and $.browser has been removed – a fast workaround
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()) && !/trident/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());



// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    document.getElementById("ctl00_onetidHeadbnnr2").src = "../Images/itclogosmall.jpg";
    document.getElementById("ctl00_onetidHeadbnnr2").width = 60;
    document.getElementById("ctl00_onetidHeadbnnr2").className = "ms-emphasis";


    // Popup versters verbergen tot ze nodig zijn
    $('#NieuwTicketPopUp').hide();
    $('#NieuwContactPopUp').hide();
    $('#validatieDialog').hide();
    $("#TicketChangePopup").hide();
    $('#BijlagenPopup').hide();
    $('#TijdsregistratiePopup').hide();

    $("#dagentxt").keyup(function (event) {
        if (event.keyCode == 13) {
            RecentGeslotenButton_Click();
        }
    });

    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    var date = new Date();

    var datep = $(".datepicker");
    datep.datepicker({
        'dateFormat': 'dd/mm/yy',
        onSelect: function () { $('#NieuwTicketUrentxt').focus(); }
    });
    $(".registratiedatepicker").datepicker({ 'dateFormat': 'dd/mm/yy' });
    datep.each(function () {
        var $d = $(this);
        $d.val(ToDateString(date.toString(), true));
    });

    $("#zoektxt").bind("mouseup", function () {
        FilterItems("");
    });

    $('#tecniekerBeschikbaarSelect').keyup(function (event) {
        var key = event.which;
        if (key === 32) {
            MovetechniekerToSelected();
        }
    });

    $('#techniekerGekozenSelect').keyup(function (event) {
        var key = event.which;
        if (key === 32) {
            MovetechniekerToAvailable()
        }
    });

    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    //$(".overlay").removeClass("Hidden");
    getUserName();
});


// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    UserEmail = user.get_title();
    $('#message').text('Hallo ' + user.get_title());
    //$('#uitvoerderLabel').text(user.get_title());
    $("#validatieDialog").prop('title', "Opgelet !!");
    $("#validatieDialog").dialog({
        modal: true,
        autoOpen: false,
        minWidth: 400,
        hide: {
            effect: "fade",
            duration: 500
        },
        show: {
            effect: "fade",
            duration: 500
        },
        buttons: {
            Ok: function () {
                $(this).dialog("close");
                $("#validatieDialog").empty();
            }
        }
    });
    var getDrempel = GetDrempelwaarde();
    getDrempel.done(function () {
        var getStatussen = GetStatussen();
        getStatussen.done(function () {
            var getBedrijfsID = GetLogedInCompany(UserEmail);
            getBedrijfsID.done(function () {
                LoadData();
            });
        });
    });
}

function LoadData()
{
    var getKlant = GetKlantenByID(UserBedrijf);
    getKlant.done(function () {
        var getContacten = GetContacten(UserBedrijf);
        getContacten.done(function () {
            var getTickets = GetTicketsByKlant(UserBedrijf);
            getTickets.done(function () {

            });
        });
    });
}

function KlantenSelect_OnChange() {
    UserBedrijf = $('#KlantenSelect option:selected').val();
    $("#recentThead").empty();
    $("#recentTbody").empty();
    $("#dagentxt").val("30");
    LoadData();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function ResetTicketFilter() {
    $("#FilterticketRow input[type=radio]").prop('checked', function () {
        return this.getAttribute('checked') === 'checked';
    });

}

function SetOpdrachtenRow() {
    var radio = $('input[name=afdelinggroup]:checked').val();
    if (radio !== "Technisch") {
        $('#opdrachtenRow').show();
    }
    else {
        $('#opdrachtenRow').hide();
    }

}

function UpdateTickets_Click() {
    GetTicketsByKlant(UserBedrijf);
}

function RemoveOldData() {
    $("#recentThead").empty();
    $("#recentTbody").empty();
    $("#klachtenThead").empty();
    $("#klachtenTbody").empty();
    $("#computersThead").empty();
    $("#computersTbody").empty();
    $("#centraleThead").empty();
    $("#centraleTbody").empty();
    $("#registratiesThead").empty();
    $("#registratiesTbody").empty();

}

function ShowNieuwContactPopup() {
    //$("#NieuwContactPopUp").prop('title', "Nieuw contact");
    $("#NieuwContactPopUp").dialog({
        width: 700,
        modal: true,
        title: "Nieuw Contact",
        hide: {
            effect: "fade",
            duration: 500
        },
        show: {
            effect: "fade",
            duration: 500
        },
        maxHeight: 700,
        buttons: [{
            text: "Opslaan",
            click: function () {
                GetContactFromForm(false);
            }
        },
            {
                text: "Annuleren",
                click: function () {
                    $(this).dialog("close");
                    ClearContact();
                }
            }
        ]
    });
}

function CloseNieuwContactPopup() {
    $("#NieuwContactPopUp").dialog('close');
    ClearContact();
}

function ClearContact() {
    $("#ContactNaamtxt").val("");
    $("#ContactVoornaamtxt").val("");
    $("#ContactFunctietxt").val("");
    $("#ContactEmailTextArea").val("");
    $("#ConatctMobieltxt").val("+32 4");
    $("#ContactTelefoonWerktxt").val("+32");
    $("#ContactTelefoonThuistxt").val("+32");
    $("#ContactBedrijftxt").val("");
    $("#ContactFaxtxt").val("");
    $("#ContactAdresTextArea").val("");
    $("#ContactPlaatstxt").val("");
    $("#ContactProvincietxt").val("");
    $("#ContactPostcodetxt").val("");
    $("#ContactLandtxt").val("");
    $("#ContactWebpaginatxt").val("http://");
    $("#ContactWebpaginaBeschrijvingtxt").val("");
    $("#ContactNotitieTextArea").val("");
    $("#ContactNaamFontxt").val("");
    $("#ContactVoornaamFontxt").val("");
    $("#ContactBedrijfsnaamFontxt").val("");
}

function ShowNieuwTicketPopup() {
    $("#NieuwTicketPopUp input[type=radio]").prop('checked', function () {
        return this.getAttribute('checked') === 'checked';
    });


    //var datum = new Date();
    //var vandaag = ToNormalDate(datum);
    //$('#NieuwTicketOpvolgDatumtxt').val(vandaag);
    ClearFileUpload();
    $('#NieuwTicketPopUp').dialog({
        width: 900,
        modal: true,
        hide: {
            effect: "fade",
            duration: 500
        },
        show: {
            effect: "fade",
            duration: 500
        },
        maxHeight: 800,
        buttons: [{
            text: "Opslaan",
            click: function () {
                GetTicketFromForm();
            }
        },
            {
                text: "Annuleren",
                click: function () {
                    $(this).dialog("close");
                    DeleteNieuwTicketData();
                    //GetTechniekers();
                    $('#ToegevoegdeTakenDiv').empty();
                    tinyMCE.get('probleemTxt').setContent("");
                    // maak nieuwTicketOpdrachten lijst leeg
                    nieuwTicketOpdrachten = [];
                }
            }
        ]
    });

    tinymce.init({
        selector: '#probleemTxt',
        language: "nl",
        theme: "modern",
        width: "550",
        height: "180",
        //strict_loading_mode: true,
        menubar: false,
        plugins: [
            "advlist autolink link image charmap hr ",
            "paste textcolor colorpicker textpattern"
        ],
        toolbar: "styleselect | bold italic | undo redo | forecolor ",
        image_advtab: true,

        resize: "both"
    });

    //LoadContacten();
}

function OpenGroupPopUp() {
    $(".overlay").removeClass("Hidden");
    $("#GroepPopup").removeClass("Hidden");
}

function HideGroupPopup() {
    $(".overlay").addClass("Hidden");
    $("#GroepPopup").addClass("Hidden");
}

function MoveToSelected() {
    var option = $("#ContactKlantSelect option:selected").clone();
    $("#SelectedKlantSelect").append(option);
}

function MoveToAvailable() {
    var option = $("#SelectedKlantSelect option:selected");
    option.remove();
}

function MovetechniekerToSelected() {
    var option = $("#tecniekerBeschikbaarSelect option:selected");
    $("#techniekerGekozenSelect").append(option);
    $("#tecniekerBeschikbaarSelect option:selected").remove();
}

function MovetechniekerToAvailable() {
    var option = $("#techniekerGekozenSelect option:selected");
    $("#tecniekerBeschikbaarSelect").append(option);
    $("#techniekerGekozenSelect option:selected").remove();
}

function MoveTitelToSelected() {
    var option = $("#titelKeuzeSelect option:selected");
    $("#titelGekozenSelect").append(option);
    $("#titelKeuzeSelect option:selected").remove();
}

function MoveTitelToAvailable() {
    var option = $("#titelGekozenSelect option:selected");
    $("#titelKeuzeSelect").append(option);
    $("#titelGekozenSelect option:selected").remove();
}

function ShowUploadContactenPopup() {
    $('#UploadContactenPopup').dialog({
        width: 950,
        modal: true,
        hide: {
            effect: "fade",
            duration: 500
        },
        show: {
            effect: "fade",
            duration: 500
        },
        height: 700,
        buttons: [{
            text: "Opslaan",
            click: function () {
                SaveImportContacten(0);
            }
        },
            {
                text: "Annuleren",
                click: function () {
                    ClearGeenVeplaatsingPopup();
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function AddItem(text, value) {
    var opt = document.createElement("option");
    opt.text = text;
    opt.value = value;
    ddl.options.add(opt);
}

function TijdsregistratieButton_Click() {
    GetAllRegistraties();
}


/**************************** Exporteren van de tickets, tijdsregistraties ********************/
function ExportTicketButton_Click() {
    var ticketArray = [];
    var headers = [];
    $("#ticketThead th").each(function (index, item) {
        headers[index] = $(item).text();
    });

    $("#ticketTbody tr").each(function () {
        //var arrayOfThisRow = [];
        var arrayItem = {};
        var tableData = $(this).find('td');
        if (tableData.length > 1) {

            tableData.each(function (index, item) {
                arrayItem[headers[index]] = $(item).text();//.push($(this).text());
            });
            ticketArray.push(arrayItem);
        }
    });
    var csv = arrayToCSV(ticketArray);
    DownloadFile(csv);
}

function ExportRegistratiesButton_Click() {
    var registratiesArray = [];
    var headers = [];
    $("#registratiesThead th").each(function (index, item) {
        headers[index] = $(item).text();
    });

    $("#registratiesTbody tr").each(function () {
        //var arrayOfThisRow = [];
        var arrayItem = {};
        var tableData = $(this).find('td');
        if (tableData.length > 1) {

            tableData.each(function (index, item) {
                arrayItem[headers[index]] = $(item).text();//.push($(this).text());
            });
            registratiesArray.push(arrayItem);
        }
    });
    var csv = arrayToCSV(registratiesArray);
    DownloadFile(csv);
}

function DownloadFile(file) {
    var blob = new Blob([file], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {
        navigator.msSaveBlob(blob, 'PlanningAppLijst.csv');
    }
    else {

        var encodedUri = URL.createObjectURL(blob);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "PlanningAppLijst.csv");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}

/************************* Filter ticketlijst op Technisch, Software of alles *******************************/
function TicketFilterRadio_CheckChanged() {
    var filtertext = "";
    var radio = $("input[name='ticketfilter']:checked");
    if (radio[0].id === "TechnischTicketRadio") {
        filtertext = "Technisch"
    }
    else if (radio[0].id === "SoftwareTicketRadio") {
        filtertext = "Software"
    }
    else {
        filtertext = "Alle";
    }

    var tabel = $("#ticketTbody tr");
    if (filtertext == "Alle") {
        tabel.show();
        return;
    }

    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        var afdeling = $t.find("td:nth-child(9)").text();
        if (afdeling.toLowerCase().indexOf(filtertext.toLowerCase()) >= 0) {
            return true;
        }
        return false;
    }).show();
}

function ClearFileUpload() {
    $("#attachmentList").empty();
    $("#fileList").empty();
    $("#addFileList").empty();
    $("#fileInput").empty();
    $("#fileInput").append(be.itc.sp.rest.fileupload.getFileInput("newTicketGetFile"));
}

function ClearPopupFileUpload() {
    $("#PopupAttachmentList").empty();
    $("#PopupFileList").empty();
    $("#PopupAddFileList").empty();
    $("#PopupFileInput").empty();
}

function ValidateOmschrijvingText() {
    var tekst = $('#NieuwTicketOmschrijvingtxt').val();
    if (tekst.length > 40) {
        alert("De tekst is te lang!");
    }
}

function ShowWijzigTicketPopup(event, id) {
    var e = event || window.event;
    var $tr = $(e.srcElement).closest('tr');
    var contact = $tr.find('td:nth-child(5)').text();
    var contacten = $('#WijzigTicketContactSelect option');
    contacten.each(function () {
        var opt = $(this);
        if (opt.text() == contact)
            $('#WijzigTicketContactSelect').val(opt.val());
    });
    $('#PopupFileInput').append(new be.itc.sp.rest.fileupload.getFileInput("popupGetFile"));
    $("#TicketChangePopup input[type=radio]").prop('checked', function () {
        return this.getAttribute('checked') === 'checked';
    });
    $("#TicketChangePopup").dialog({
        width: 820,
        modal: true,
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        hide: {
            effect: "fade",
            duration: 400
        },
        show: {
            effect: "fade",
            duration: 400
        },
        height: 630,
        buttons: [{
            text: "Opslaan",
            click: function () {
                SaveChanges(id);
            }
        },
            {
                text: "Annuleren",
                click: function () {
                    ClearWijzigTicket();
                    
                    $(this).dialog("close");
                }
            }
        ]
    });

    tinymce.init({
        selector: '#WijzidOpmerkingTextArea',
        language: "nl",
        theme: "modern",
        width: "550",
        height: "180",
        //strict_loading_mode: true,
        menubar: false,
        plugins: [
            "advlist autolink link image charmap hr ",
            "paste textcolor colorpicker textpattern"
        ],
        toolbar: "styleselect | bold italic | undo redo | forecolor ",
        image_advtab: true,

        resize: "both"
    });
}

function ClearWijzigTicket() {
    $("#WijzigTicketContactSelect > option").eq(0).attr('selected', 'selected');
    //$('#WijzidOpmerkingTextArea').val("");
    tinyMCE.get('WijzidOpmerkingTextArea').setContent("");
    ClearPopupFileUpload();
}

function ClearPopupFileUpload() {
    $("#PopupAttachmentList").empty();
    $("#PopupFileList").empty();
    $("#PopupAddFileList").empty();
    $("#PopupFileInput").empty();
}

function ShowBijlagePopup(id) {
    be.itc.sp.rest.fileupload.showAttachments(id);
    $('#BijlagenPopup').dialog({
        width: 700,
        modal: true,
        open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
        hide: {
            effect: "fade",
            duration: 400
        },
        show: {
            effect: "fade",
            duration: 400
        },
        //height: 500,
        buttons: [
            {
                text: "OK",
                click: function () {
                    $(this).dialog("close");
                    $('#bijlageContainer').empty();
                }
            }
        ]
    });
}
