﻿<%@ Page language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming" runat="server" />

<html>
<head>
    <title></title>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/jquery-ui-1.11.4.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script src="../Scripts/jquery.dataTables.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.widgets.min.js"></script>
    <script src="../Scripts/DatumFuncties.js"></script>
    <script src="../Scripts/Office/1/office.js"></script>
    <script src="../Scripts/be.itc.number.js"></script>
    <script src="../Scripts/be.itc.date.js"></script>
    <script src="../Scripts/be.itc.sp.js"></script>
    <script src="../Scripts/be.itc.sp.klant.js"></script>
    <script src="../Scripts/be.itc.sp.planning.appointment.js"></script>
    <script src="../Scripts/be.itc.sp.rest.js"></script>
    <script src="../Scripts/be.itc.sp.ticket.js"></script>
    <script src="../Scripts/be.itc.sp.user.js"></script>
    <script src="../Scripts/Map.js"></script>
    <script src="../Scripts/Models/LogedInCompanyViewModel.js"></script>
    <script src="../Scripts/be.itc.sp.exception.js"></script>
    <script src="../Scripts/be.itc.sp.rest.fileupload.js"></script>
    <script src="../Scripts/Models/OnderhoudViewModel.js"></script>
    <script src="../Scripts/Models/DrempelwaardeViewModel.js"></script>
    <script src="../Scripts/Models/StatusViewModel.js"></script>
    <script src="../Scripts/Models/UsersViewModel.js"></script>
    <script src="../Scripts/Models/KlantenViewModel.js"></script>
    <script src="../Scripts/Models/TicketViewModel.js"></script>
    <script src="../Scripts/Models/ContactenViewModel.js"></script>
    <script src="../Scripts/Models/TijdsregistratieViewModel.js"></script>
    <script src="../Scripts/datepicker-nl.js"></script>
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link href="../Content/jquery-ui-theme/jquery-ui.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.structure.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.theme.css" rel="stylesheet" />
    <script src="../Scripts/App.js"></script>

<%--    <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.
        (function () {
            'use strict';

            var hostUrl = '';
            if (document.URL.indexOf('?') != -1) {
                var params = document.URL.split('?')[1].split('&');
                for (var i = 0; i < params.length; i++) {
                    var p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split('=')[1];
                        document.write('<link rel="stylesheet" href="' + hostUrl + '/_layouts/15/defaultcss.ashx" />');
                        break;
                    }
                }
            }
            if (hostUrl == '') {
                document.write('<link rel="stylesheet" href="/_layouts/15/1033/styles/themable/corev15.css" />');
            }
        })();
    </script>--%>
</head>
<body>
    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
        <div class="InfoDiv">
            <div id="klantDetailsDiv">
                <h3>
                    Klant
                    <a href="#" onclick="return false" class="NewImage" title="Nieuw contact"><img /></a>
                </h3>              
                <div class="stap KlantDiv">
                    <div class="klantPart">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="labels">ID:</td>
                                    <td id="IDCell"></td>
                                    <%--<td class="labels">Opmerking facturatie:</td>
                                    <td class="labels">Opmerking klant:</td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Bedrijf:</td>
                                    <td id="bedrijfCell"></td>
        <%--                            <td rowspan="6" style="width: 300px;">
                                        <div style="overflow-y:auto;">
                                            <label id="facturatieCell"  />
                                        </div>
                                    </td>
                                    <td id="opmklantCell" rowspan="3"></td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Adres:</td>
                                    <td id="adresCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Telefoon:</td>
                                    <td id="telefoonCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Hosting:</td>
                                    <td id="hostingCell"></td>
                                    <%--<td class="labels">Opmerking locatie:</td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Vertegenwoordiger:</td>
                                    <td id="vertegenwoordigertCell"></td>
        <%--                            <td rowspan="4" style="width: 300px;" id="opmLocatieCell">
                                
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Groep:</td>
                                    <td id="groepCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Technieker:</td>
                                    <td id="techniekerCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Onderhoud:</td>
                                    <td id="volgendonderhoudCell" colspan="2"></td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
            <div id="contactenDiv">
                <h3>
                    Contacten 
                    <a href="#" onclick="ShowNieuwContactPopup(true);" class="NewImage" title="Nieuw contact"><img src="../Images/new.png" alt="Nieuw" title="Nieuw contact" /></a>
                </h3>
                <div class="stap KlantDiv">
                    <table>
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Naam</td>
                                <td>Telefoon</td>
                                <td>GSM</td>
                                <td>E-mail</td>
                            </tr>
                        </thead>
                        <tbody id="contactTbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="clear: left;"><p></p></div>  
        <h3>
            Tickets 
            <a href="#" onclick="ShowNieuwTicketPopup(true);" class="NewImage"><img src="../Images/new.png" alt="Nieuw" title="Nieuw ticket" /></a>
        </h3>
        <div class="stap">
            <table>
                <tbody>
                    <tr>
                        <td>Vernieuw de ticketlijst:</td>
                        <td><a href="#" class="Buttons" onclick="UpdateTickets_Click();">Update</a></td>
                        <td><a href="#" class="Buttons" onclick="Groupticket_Click();">Toon groep tickets</a></td>
                    </tr>
                    <tr id="FilterticketRow">
                        <td>Zoek:</td>
                        <td>
                            <input type="text" id="ticketzoektxt" onkeyup="FilterTickets();" /></td>
                        <td>
                            <input type="radio" checked="checked" name="ticketfilter" id="AlleTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Alle</td>
                        <td>
                            <input type="radio" name="ticketfilter" id="TechnischTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Technisch</td>
                        <td>
                            <input type="radio" name="ticketfilter" id="SoftwareTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Software</td>
                    </tr>
                </tbody>
            </table>
            <div id="ticketDiv">
                <table id="ticketTable">
                    <thead id="ticketThead">
                        <tr>
                            <th>Nummer</th>
                            <th>Omschrijving</th>
                            <th>Prioriteit</th>
                            <th>Toegewezen aan</th>
                            <th>Contact persoon</th>
                            <th>Opvolgingsdatum</th>
                            <th>Status</th>
                            <th>Werkgebied</th>
                            <th>Afdeling</th>
                            <th>Laatst gewijzigd</th>
                            <th>Door</th>
                        </tr>
                    </thead>
                    <tbody id="ticketTbody">
                    </tbody>
                </table>
            </div>
            <p></p>
        </div>
        <div class="overlay Hidden">
        </div>
        <div id="NieuwContactPopUp" title="Nieuw contact">
            <div style="overflow-y: auto; height: 94%;">
                <input id="contactIdHidden" type="hidden" value="0" />
                <fieldset>
                    <label class="labelFor">Achternaam *</label>
                    <input type="text" id="ContactNaamtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Voornaam *</label>
                    <input type="text" id="ContactVoornaamtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Functie</label>
                    <input type="text" id="ContactFunctietxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">E-mailadres</label>
                    <textarea id="ContactEmailTextArea" class="nieuwtextarea"></textarea>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Mobiel nummer</label>
                    <input type="text" id="ConatctMobieltxt" value="+32 4" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Telefoon werk</label>
                    <input type="text" id="ContactTelefoonWerktxt" value="+32" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Telefoon thuis</label>
                    <input type="text" id="ContactTelefoonThuistxt" value="+32" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bedrijf</label>
                    <input type="text" id="ContactBedrijftxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Fax nummer</label>
                    <input type="text" id="ContactFaxtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Adres</label>
                    <textarea id="ContactAdresTextArea" class="nieuwtextarea"></textarea>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Plaats</label>
                    <input type="text" id="ContactPlaatstxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Provincie</label>
                    <input type="text" id="ContactProvincietxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Postcode</label>
                    <input type="text" id="ContactPostcodetxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Land/regio</label>
                    <input type="text" id="ContactLandtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Webpagina</label>
                    <input type="text" id="ContactWebpaginatxt" value="http://" class="nieuwTextBox" /><br />
                    <label class="labelFor">Typ de beschrijving:</label>
                    <input type="text" id="ContactWebpaginaBeschrijvingtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Achternaam (fonetisch)</label>
                    <input type="text" id="ContactNaamFontxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Voornaam (fonetisch)</label>
                    <input type="text" id="ContactVoornaamFontxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bedrijfsnaam (fonetisch)</label>
                    <input type="text" id="ContactBedrijfsnaamFontxt" class="nieuwTextBox" />
                </fieldset>
            </div>
        </div>
        <div id="NieuwTicketPopUp" title="Nieuw ticket">
            <div style="overflow-y: auto; height: 94%;">
                <fieldset>
                    <Label class="labelFor" for="NieuwTicketContactSelect">Contact *</Label>
                    <select id="NieuwTicketContactSelect" style="width: 400px;"></select>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="NieuwTicketOmschrijvingtxt">Korte omschrijving *</label>
                    <input type="text" id="NieuwTicketOmschrijvingtxt" onkeyup="ValidateOmschrijvingText();" class="nieuwTextBox" /><br />
                    <label class="divFor bijTekst">Factuur tekst, dus geen "thuis", "privé", telefoonnummer, opmerkingen ...</label>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Probleem omschrijving *</label>
                    <textarea rows="4" cols="60" id="probleemTxt"></textarea>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="NieuwTicketWerkgebiedSelect">Werkgebied *</label>
                    <select id="NieuwTicketWerkgebiedSelect">
                        <option></option>
                        <option>Backup</option>
                        <option>Briljant</option>
                        <option>Citrix</option>
                        <option>Cloud</option>
                        <option>Firewall</option>
                        <option>Hard Disk</option>
                        <option>Hardware</option>
                        <option>Hosting</option>
                        <option>Internet</option>
                        <option>Laptop/desktop</option>
                        <option>Licentie/certificaat</option>
                        <option>Kaseya</option>
                        <option>Mail</option>
                        <option>Mobile</option>
                        <option>Onderhoud</option>
                        <option>Philips Dictate</option>
                        <option>Printer</option>
                        <option>SharePoint</option>
                        <option>Server</option>
                        <option>Sessie lock</option>
                        <option>Software</option>
                        <option>Spam</option>
                        <option>Telecom</option>
                        <option>User account</option>
                        <option>Virus/Spyware</option>
                        <option>Andere</option>
                        <option>Documentatie</option>
                        <option>WerkStatus</option>
                    </select>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="tecniekerBeschikbaarSelect">Toegewezen aan *</label>
                    <select id="tecniekerBeschikbaarSelect" style="width: 150px;">
                        <option value="108">Helpdesk ITC Belgium</option>
                        <option value="130" selected="selected">Planning ITC Belgium</option>
                    </select>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="typegroup">Type ticket *</label>
                    <div class="divFor">
                        <input type="radio" name="typegroup" checked="checked" id="NieuwTicketCallRadio" />CALL<br />
                        <input type="radio" name="typegroup" id="NieuwTicketTaskRadio" />TASK<br />
                        <input type="radio" name="typegroup" id="NieuwTicketProjectRadio" />PROJECT
                    </div>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bijlagen</label>
                    <div class="divFor">
                        <!--File Upload -->
                        <div id="attachmentList"></div>
                        <div id="fileList"></div>
                        <div id="addFileList"></div>
                        <div id="fileInput"></div>
                    </div>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Afdeling</label>
                    <div class="divFor">
                        <input type="radio" name="afdelinggroup" checked="checked" id="NieuwTickettechnischRadio" onchange="SetOpdrachtenRow();" value="Technisch"/>Technisch<br />
                        <input type="radio" name="afdelinggroup" id="NieuwTicketSoftwareRadio" onchange="SetOpdrachtenRow();" value="Software"/>Software<br />
                        <input type="radio" name="afdelinggroup" id="NieuwTickettechnischSoftwareRadio" onchange="SetOpdrachtenRow();" value="Technisch en software"/>Technisch en software<br />
                        <label class="bijTekst">* Technisch: Dit ticket is voor de technische dienst. De software afdeling moet hier niets voor doen.<br />
                        * Software: dit ticket is voor de software afdeling. Het gaat hier over een probleem ontstaan door geschreven software door ITC/INNI.<br /> 
                        * Technisch en software: Zowel de technische dienst als de software afdeling zijn betrokken!</label>
                    </div>
                </fieldset>
            </div>
        </div>
        <div id="TijdsregistratiePopup" title="Ticket">
            <table>
                <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Omschrijving</th>
                        <th>Onsite</th>
                        <th>Pemanentie</th>
                        <th>Gemaakt door</th>
                    </tr>
                </thead>
                <tbody id="tijdsregistratieTBody">

                </tbody>
            </table>
        </div>
        <div id="validatieDialog" title="Opgelet!">
        </div>
</body>
</html>
