﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/jquery-ui-1.11.4.min.js"></script>
    <script src="/_layouts/15/sp.runtime.js"></script>
    <script src="/_layouts/15/sp.js"></script>
    <script src="../Scripts/jquery.dataTables.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.widgets.min.js"></script>
    <script src="../Scripts/DatumFuncties.js"></script>
    <script src="../Scripts/Office/1/office.js"></script>
    <script src="../Scripts/be.itc.number.js"></script>
    <script src="../Scripts/be.itc.date.js"></script>
    <script src="../Scripts/be.itc.sp.js"></script>
    <script src="../Scripts/be.itc.sp.klant.js"></script>
    <script src="../Scripts/be.itc.sp.planning.appointment.js"></script>
    <script src="../Scripts/be.itc.sp.rest.js"></script>
    <script src="../Scripts/be.itc.sp.ticket.js"></script>
    <script src="../Scripts/be.itc.sp.user.js"></script>
    <script src="../Scripts/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="../Scripts/Map.js"></script>
    <script src="../Scripts/Models/LogedInCompanyViewModel.js"></script>
    <script src="../Scripts/be.itc.sp.exception.js"></script>
    <script src="../Scripts/be.itc.sp.rest.fileupload.js"></script>
    <script src="../Scripts/Models/RecentGeslotenViewModel.js"></script>
    <script src="../Scripts/Models/OnderhoudViewModel.js"></script>
    <script src="../Scripts/Models/DrempelwaardeViewModel.js"></script>
    <script src="../Scripts/Models/StatusViewModel.js"></script>
    <script src="../Scripts/Models/UsersViewModel.js"></script>
    <script src="../Scripts/Models/KlantenViewModel.js"></script>
    <script src="../Scripts/Models/TicketViewModel.js"></script>
    <script src="../Scripts/Models/ContactenViewModel.js"></script>
    <script src="../Scripts/Models/TijdsregistratieViewModel.js"></script>
    <script src="../Scripts/datepicker-nl.js"></script>
    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <%--<link href="../Content/appointment.css" rel="stylesheet" />--%>
    <link href="../Content/jquery-ui-theme/jquery-ui.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.structure.css" rel="stylesheet" />
    <link href="../Content/jquery-ui-theme/jquery-ui.theme.css" rel="stylesheet" />
    <!-- Add your JavaScript to the following file -->
    <script src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Klant Ticket App
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <a href="https://itcbe.sharepoint.com/klanten">Terug naar Home</a>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
    <div>
        Locatie: <select id="KlantenSelect" onchange="KlantenSelect_OnChange();"></select>
        <div class="InfoDiv">        
            <div id="klantDetailsDiv">
                <h3>
                    Klant
                    <a href="#" onclick="return false" class="NewImage" title="Nieuw contact"><img /></a>
                </h3>              
                <div class="stap KlantDiv">
                    <div>
                        <table>
                            <tbody>
                                <tr>
                                    <td class="labels">ID:</td>
                                    <td id="IDCell"></td>
                                    <%--<td class="labels">Opmerking facturatie:</td>
                                    <td class="labels">Opmerking klant:</td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Bedrijf:</td>
                                    <td id="bedrijfCell"></td>
        <%--                            <td rowspan="6" style="width: 300px;">
                                        <div style="overflow-y:auto;">
                                            <label id="facturatieCell"  />
                                        </div>
                                    </td>
                                    <td id="opmklantCell" rowspan="3"></td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Adres:</td>
                                    <td id="adresCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Telefoon:</td>
                                    <td id="telefoonCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Hosting:</td>
                                    <td id="hostingCell"></td>
                                    <%--<td class="labels">Opmerking locatie:</td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Vertegenwoordiger:</td>
                                    <td id="vertegenwoordigertCell"></td>
        <%--                            <td rowspan="4" style="width: 300px;" id="opmLocatieCell">
                                
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td class="labels">Groep:</td>
                                    <td id="groepCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Technieker:</td>
                                    <td id="techniekerCell"></td>
                                </tr>
                                <tr>
                                    <td class="labels">Onderhoud:</td>
                                    <td id="volgendonderhoudCell"></td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
            <div id="contactenDiv">
                <h3>
                    Contacten 
                    <a href="#" onclick="ShowNieuwContactPopup(true);" class="NewImage" title="Nieuw contact"><img src="../Images/new.png" alt="Nieuw" title="Nieuw contact" /></a>
                </h3>
                <div class="stap KlantDiv">
                    <table>
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Naam</td>
                                <td>Telefoon</td>
                                <td>GSM</td>
                                <td>E-mail</td>
                            </tr>
                        </thead>
                        <tbody id="contactTbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="clear: left;"><p></p></div>  
        <h3>
            Openstaande tickets 
            <a href="#" onclick="ShowNieuwTicketPopup(true);" class="NewImage"><img src="../Images/new.png" alt="Nieuw" title="Nieuw ticket" /></a>
        </h3>
        <div class="stap">
            <table>
                <tbody>
                    <tr>
                        <td>Vernieuw de ticketlijst:</td>
                        <td><a href="#" class="Buttons" onclick="UpdateTickets_Click();">Update</a></td>
                        <td colspan="2"><a href="#" class="Buttons" onclick="Groupticket_Click();">Toon groep tickets</a></td>
                    </tr>
                    <tr id="FilterticketRow">
                        <td>Zoek:</td>
                        <td>
                            <input type="text" id="ticketzoektxt" onkeyup="FilterTickets();" /></td>
                        <td>
                            <input type="radio" checked="checked" name="ticketfilter" id="AlleTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Alle</td>
                        <td>
                            <input type="radio" name="ticketfilter" id="TechnischTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Technisch</td>
                        <td>
                            <input type="radio" name="ticketfilter" id="SoftwareTicketRadio" onclick="TicketFilterRadio_CheckChanged();" />Software</td>
                    </tr>
                </tbody>
            </table>
            <div id="ticketDiv">
                <table id="ticketTable">
                    <thead id="ticketThead">
                        <tr>
                            <th>Nummer</th>
                            <th>Omschrijving</th>
                            <th>Prioriteit</th>
                            <th>Toegewezen aan</th>
                            <th>Contact persoon</th>
                            <th>Opvolgingsdatum</th>
                            <th>Status</th>
                            <th>Werkgebied</th>
                            <th>Afdeling</th>
                            <th>Laatst gewijzigd</th>
                            <th>Door</th>
                        </tr>
                    </thead>
                    <tbody id="ticketTbody">
                    </tbody>
                </table>
            </div>
            <p></p>
        </div>
        <h3>
            Recent gesloten tickets
        </h3>
        <div class="stap">
             Hoeveel dagen wilt U terug gaan:
            <input type="text" id="dagentxt" value="30" />
            <a href="#" class="Buttons" onclick="RecentGeslotenButton_Click()" title="Toon recent gesloten tickets">Filter</a>
            <table>
                <thead id="recentThead">
                </thead>
                <tbody id="recentTbody">
                </tbody>
            </table>                                                                                                                                    
        </div>
        <div class="overlay Hidden">
        </div>
        <div id="NieuwContactPopUp" title="Nieuw contact" class="popups">
            <div style="overflow-y: auto; height: 94%;">
                <input id="contactIdHidden" type="hidden" value="0" />
                <fieldset>
                    <label class="labelFor">Achternaam *</label>
                    <input type="text" id="ContactNaamtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Voornaam *</label>
                    <input type="text" id="ContactVoornaamtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Functie</label>
                    <input type="text" id="ContactFunctietxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">E-mailadres</label>
                    <textarea id="ContactEmailTextArea" class="nieuwtextarea"></textarea>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Mobiel nummer</label>
                    <input type="text" id="ConatctMobieltxt" value="+32 4" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Telefoon werk</label>
                    <input type="text" id="ContactTelefoonWerktxt" value="+32" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Telefoon thuis</label>
                    <input type="text" id="ContactTelefoonThuistxt" value="+32" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bedrijf</label>
                    <input type="text" id="ContactBedrijftxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Fax nummer</label>
                    <input type="text" id="ContactFaxtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Adres</label>
                    <textarea id="ContactAdresTextArea" class="nieuwtextarea"></textarea>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Plaats</label>
                    <input type="text" id="ContactPlaatstxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Provincie</label>
                    <input type="text" id="ContactProvincietxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Postcode</label>
                    <input type="text" id="ContactPostcodetxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Land/regio</label>
                    <input type="text" id="ContactLandtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Webpagina</label>
                    <input type="text" id="ContactWebpaginatxt" value="http://" class="nieuwTextBox" /><br />
                    <label class="labelFor">Beschrijving website</label>
                    <input type="text" id="ContactWebpaginaBeschrijvingtxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Achternaam (fonetisch)</label>
                    <input type="text" id="ContactNaamFontxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Voornaam (fonetisch)</label>
                    <input type="text" id="ContactVoornaamFontxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bedrijfsnaam (fonetisch)</label>
                    <input type="text" id="ContactBedrijfsnaamFontxt" class="nieuwTextBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Uit dienst</label>
                    <input type="checkbox" id="uitDienstCheckBox" />
                </fieldset>
            </div>
        </div>
        <div id="NieuwTicketPopUp" title="Nieuw ticket" class="popups">
            <div style="overflow-y: auto; height: 94%;">
                <fieldset>
                    <Label class="labelFor" for="NieuwTicketContactSelect">Contact *</Label>
                    <select id="NieuwTicketContactSelect" style="width: 400px;"></select>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="NieuwTicketOmschrijvingtxt">Korte omschrijving *</label>
                    <input type="text" id="NieuwTicketOmschrijvingtxt" onkeyup="ValidateOmschrijvingText();" class="nieuwTextBox" /><br />
                </fieldset>
                <fieldset>
                    <label class="labelFor">Probleem omschrijving *</label>
                    <div style="margin-left:204px">
                        <textarea rows="15" cols="100" id="probleemTxt"></textarea>
                    </div>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="NieuwTicketWerkgebiedSelect">Werkgebied *</label>
                    <select id="NieuwTicketWerkgebiedSelect">
                        <option></option>
                        <option>Backup</option>
                        <option>Briljant</option>
                        <option>Citrix</option>
                        <option>Cloud</option>
                        <option>Documentatie</option>
                        <option>Firewall</option>
                        <option>Hard Disk</option>
                        <option>Hardware</option>
                        <option>Hosting</option>
                        <option>Internet</option>
                        <option>Laptop/desktop</option>
                        <option>Licentie/certificaat</option>
                        <option>Kaseya</option>
                        <option>Mail</option>
                        <option>Mobile</option>
                        <option>Onderhoud</option>
                        <option>Philips Dictate</option>
                        <option>Printer</option>
                        <option>SharePoint</option>
                        <option>Server</option>
                        <option>Sessie lock</option>
                        <option>Software</option>
                        <option>Spam</option>
                        <option>Telecom</option>
                        <option>User account</option>
                        <option>Virus/Spyware</option>
                        <option>Andere</option>                        
                    </select>
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="tecniekerBeschikbaarSelect">Toegewezen aan *</label>
                    <select id="tecniekerBeschikbaarSelect" style="width: 150px;">
                        <option value="108">Helpdesk ITC Belgium</option>
                        <option value="130" selected="selected">Planning ITC Belgium</option>
                    </select>
                </fieldset>
                <fieldset id="prioriteitFieldSet">
                    <label class="labelFor" for="typegroup">Prioriteit *</label>
                    <select id="prioriteitSelect" style="width: 150px">
                        <option value="1 Laag">1 Laag</option>
                        <option value="2 Gemiddeld" selected="selected">2 Gemiddeld</option>
                        <option value="3 Hoog">3 Hoog</option>
                        <option value="4 Zeer hoog">4 Zeer hoog</option>
                    </select>
                </fieldset>
                <fieldset>
                    <label class="labelFor">Bijlagen</label>
                    <div class="divFor">
                        <!--File Upload -->
                        <div id="attachmentList"></div>
                        <div id="fileList"></div>
                        <div id="addFileList"></div>
                        <div id="fileInput"></div>
                    </div>
                </fieldset>
<%--                <fieldset>
                    <label class="labelFor">Afdeling</label>
                    <div class="divFor">
                        <input type="radio" name="afdelinggroup" checked="checked" id="NieuwTickettechnischRadio" onchange="SetOpdrachtenRow();" value="Technisch"/>Technisch<br />
                        <input type="radio" name="afdelinggroup" id="NieuwTicketSoftwareRadio" onchange="SetOpdrachtenRow();" value="Software"/>Software<br />
                        <input type="radio" name="afdelinggroup" id="NieuwTickettechnischSoftwareRadio" onchange="SetOpdrachtenRow();" value="Technisch en software"/>Technisch en software<br />
                        <label class="bijTekst">* Technisch: Dit ticket is voor de technische dienst. De software afdeling moet hier niets voor doen.<br />
                        * Software: dit ticket is voor de software afdeling. Het gaat hier over een probleem ontstaan door geschreven software door ITC/INNI.<br /> 
                        * Technisch en software: Zowel de technische dienst als de software afdeling zijn betrokken!</label>
                    </div>
                </fieldset>--%>
            </div>
        </div>
        <div id="TijdsregistratiePopup" title="Ticket"  class="popups">
            <table>
                <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Omschrijving</th>
                        <th>Onsite</th>
                        <th>Pemanentie</th>
                        <th>Gemaakt door</th>
                    </tr>
                </thead>
                <tbody id="tijdsregistratieTBody">

                </tbody>
            </table>
        </div>
        <div id="TicketChangePopup" title="Wijzig ticket" class="popups">
            <fieldset>
                <Label class="labelFor" for="WijzigTicketContactSelect">Contact</Label>
                <select id="WijzigTicketContactSelect" style="width: 400px;"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor">Opmerking</label>
                <div style="margin-left:204px;">
                    <textarea id="WijzidOpmerkingTextArea" rows="20" cols="100"></textarea>
                </div>
            </fieldset>
            <fieldset>
                <label class="labelFor">Bijlagen</label>
                <div class="divFor">
                    <!--File Upload -->
                    <div id="PopupAttachmentList"></div>
                    <div id="PopupFileList"></div>
                    <div id="PopupAddFileList"></div>
                    <div id="PopupFileInput"></div>
                </div>
            </fieldset>
            <fieldset>
                <label class="labelFor">Toewijzen aan</label>
                <div class="divFor">
                    <input id="GeenActieRadio" type="radio" checked="checked" name="statusRadio" />
                    <label for="GeenActieRadio">Geen actie</label><br />
                    <input id="TeplannenRadio" type="radio" name="statusRadio" />
                    <label for="TeplannenRadio">In te plannen</label><br />
                    <input id="HelpdeskRadio" type="radio" name="statusRadio" />
                    <label for="HelpdeskRadio">Toewijzen helpdesk</label>
                </div>
            </fieldset>
        </div>
        <div id="BijlagenPopup" title="Bijlagen" class="popups">
            <div id="bijlageContainer"></div>
        </div> 
        <div id="validatieDialog" title="Opgelet!">
        </div>
    </div>
</asp:Content>
